USE [GestioneArbitri]
GO
/****** Object:  Table [dbo].[Arbitro]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Arbitro](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tessera] [int] NOT NULL,
	[Cognome] [nvarchar](50) NOT NULL,
	[Nome] [nvarchar](50) NOT NULL,
	[Categoria] [varchar](10) NOT NULL,
	[Qualifica] [nvarchar](100) NOT NULL,
	[Abilitazione] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[NPartiteArbitrate] [int] NULL,
 CONSTRAINT [PK_AnagraficaArbitri] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ArbitroEccezione]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ArbitroEccezione](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ArbitroId] [int] NOT NULL,
	[EccezioneId] [int] NOT NULL,
 CONSTRAINT [PK_ArbitroEccezione] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Calendario]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Calendario](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Girone] [varchar](3) NOT NULL,
	[Campionato] [varchar](15) NOT NULL,
	[Categoria] [varchar](15) NOT NULL,
	[TipoGiornata] [varchar](10) NOT NULL,
	[Data] [date] NOT NULL,
	[SquadraCasaId] [int] NOT NULL,
	[SquadraOspiteId] [int] NOT NULL,
	[Arbitro1Id] [int] NULL,
	[Arbitro2Id] [int] NULL,
	[Campo] [nvarchar](50) NOT NULL,
	[Indirizzo] [nvarchar](100) NOT NULL,
	[GiornoPartita] [varchar](10) NOT NULL,
	[Citta] [varchar](50) NOT NULL,
	[Provincia] [varchar](20) NOT NULL,
	[Ora] [nvarchar](8) NOT NULL,
	[Giornata] [int] NOT NULL,
 CONSTRAINT [PK_Calendario] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Eccezione]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Eccezione](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SquadraId] [int] NULL,
	[DataInizio] [date] NULL,
	[DataFine] [date] NULL,
	[Note] [nvarchar](max) NULL,
 CONSTRAINT [PK_Eccezioni] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Squadra]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Squadra](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NomeSquadra] [nvarchar](100) NOT NULL,
	[Girone] [varchar](3) NOT NULL,
	[Categoria] [varchar](15) NOT NULL,
 CONSTRAINT [PK_AnagraficaSquadre] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Utente]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Utente](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nome] [nvarchar](50) NOT NULL,
	[Cognome] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](20) NOT NULL,
	[Ruolo] [varchar](10) NOT NULL,
 CONSTRAINT [PK_AnagraficaUtenti] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Arbitro] ON 

INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (1, 56274, N'Aloisi', N'Fabio', N'Silver', N'Arbitro Regionale
', N'Serie C
', N'fabiotto_96@hotmail.it
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (4, 48687, N'Aloisi', N'Marco', N'Gold', N'Arbitro Regionale
', N'Serie C', N'marco_alo@hotmail.it
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (5, 57987, N'Amato', N'Matteo', N'Silver', N'Arbitro Regionale
', N'Serie C', N'matteo.amato97@libero.it
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (6, 63339, N'Amorosi
', N'Andrea
', N'Silver', N'Arbitro Regionale
', N'Serie C
', N'leonardo.amorosi2001@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (7, 57649, N'Andreola', N'Nicola Giulio', N'Silver', N'Arbitro Regionale
', N'Seric C', N'nicolandreola@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (8, 44255, N'Antignano
', N'Gaetano', N'Silver', N'Arbitro Regionale', N'Serie C', N'gaetano.antignano@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (9, 62101, N'Ascani', N'Nicholas', N'Silver', N'Arbitro Regionale
', N'Serie C
', N'nicholas.ascani@libero.it
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (10, 59156, N'Augelli
', N'Francesco', N'Silver', N'Arbitro Regionale
', N'Serie C', N'francescoaug@hotmail.it
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (11, 46987, N'Bernardo', N'Lucia', N'Gold', N'Arbitro Regionale
', N'Serie C con deroga A2/F
', N'lucia_bernardo@libero.it
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (12, 60087, N'Bertoni', N'Matteo', N'Silver', N'Arbitro Regionale
', N'Serie C', N'bertoni.matteo00@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (13, 44903, N'Bova', N'Remigio', N'Gold', N'Arbitro Regionale', N'Serie C', N'remix46@live.it
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (14, 61482, N'Capatan', N'Valentina Martina', N'Silver', N'Arbitro Regionale ', N'Serie C', N'valentinacapatan01@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (15, 59250, N'Caracciolo', N'Laura', N'Silver', N'Arbitro Regionale', N'Serie C', N'laura.caracciolo94@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (16, 46372, N'Carotenuto', N'Davide', N'Silver', N'Arbitro Regionale', N'Serie C', N'dadocaro@hotmail.it
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (17, 54830, N'Catania', N'Giuseppe', N'Silver', N'Arbitro Regionale', N'Serie C', N'7peppe7@live.it
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (18, 54854, N'Ciaccioni', N'Lorenzo', N'Gold', N'Arbitro Regionale', N'Serie C', N'lorenzo.ciaccioni@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (19, 57993, N'Cicerchia', N'Alessandro', N'Gold', N'Arbitro Regionale ', N'Serie C', N'alessandrocicerchia00@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (20, 59441, N'Coda', N'Mattia', N'Gold', N'Arbitro Regionale', N'Serie C', N'mattia.coda.mc@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (21, 34031, N'Collura', N'Pierluigi', N'Gold', N'Arbitro Nazionale', N'Serie C', N'pierluigi.collura@yahoo.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (22, 61209, N'Corcione', N'Flavio Domenico', N'Silver', N'Arbitro Regionale', N'Serie C', N'flaviodcorcione@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (23, 36763, N'De Mattia', N'Valerio', N'Silver', N'Arbitro Nazionale', N'Serie C', N'valerio.demattia@hotmail.it
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (24, 48032, N'Delle Cese', N'Paolo', N'Silver', N'Arbitro Regionale', N'Serie C', N'pabasket@hotmail.it
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (25, 34593, N'Desideri', N'Emanuele', N'Gold', N'Arbitro Nazionale', N'Serie C', N'e.desideri@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (26, 67232, N'Di Gennaro', N'Marco', N'Silver', N'Arbitro Regionale', N'Serie C', N'annonovanta4@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (27, 62102, N'Dinoi', N'Federico', N'Gold', N'Arbitro Regionale', N'Serie C', N'federicodinoi@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (28, 55705, N'D''Intino', N'Leonardo', N'Silver', N'Arbitro Regionale', N'Serie C', N'leoroma94@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (29, 52009, N'Falamesca', N'Mattia', N'Gold', N'Arbitro Regionale', N'Serie C', N'ma97ti@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (30, 53955, N'Faustini', N'Sara', N'Silver', N'Arbitro Regionale', N'Serie C', N'saretta_basket98@yahoo.it
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (31, 65875, N'Formica', N'Lorenzo', N'Silver', N'Arbitro Regionale', N'Serie C', N'lolloformic@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (32, 41214, N'Fornaro', N'Alessandro', N'Gold', N'Arbitro Nazionale', N'Serie C', N'sandro1990@hotmail.it
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (33, 57648, N'Gismondi', N'Alessio', N'Silver', N'Arbitro Regionale', N'Serie C', N'gisale1994@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (34, 63283, N'Hegedus', N'Luca', N'Silver', N'Arbitro Regionale', N'Serie C', N'ermejodebasket@yahoo.it
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (35, 64441, N'Iantosca
', N'Guglielmo', N'Silver', N'Arbitro Regionale', N'Serie C', N'gully17112002@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (36, 61207, N'Iurato', N'Carlo', N'Gold', N'Arbitro Regionale', N'Serie C', N'carloiurato@virgilio.it
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (37, 51353, N'Lilli', N'Matteo', N'Gold', N'Arbitro Regionale', N'Serie C', N'matteo.lilli6991@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (38, 34964, N'Lorenzoni', N'Stefano', N'Silver', N'Arbitro Regionale', N'Serie C', N's.lorenzoni@hotmail.it
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (39, 58010, N'Lucarelli', N'Matteo', N'Silver', N'Arbitro Regionale', N'Serie C', N'matteolucarelli89@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (40, 53336, N'Marcocchia', N'Andrea', N'Silver', N'Arbitro Regionale', N'Serie C', N'andrea.marcocchia@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (41, 53344, N'Marino', N'Simone', N'Gold', N'Arbitro Regionale', N'Serie C', N'simone.mrn@hotmail.it
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (42, 59259, N'Matrigiani', N'Livio', N'Gold', N'Arbitro Regionale', N'Serie C', N'livio.matrigiani@libero.it
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (43, 54693, N'Matteis', N'Simone', N'Silver', N'Arbitro Regionale', N'Serie C', N'simat97@alice.it
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (44, 61841, N'Medici', N'Laura', N'Silver', N'Arbitro Regionale', N'Serie C', N'laurajmedici@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (45, 60093, N'Ottaviani', N'Francesco', N'Silver', N'Arbitro Regionale', N'Serie C', N'francy.3131@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (46, 53341, N'Ottoveggio', N'Alessandro', N'Silver', N'Arbitro Regionale', N'Serie C', N'ottoveggioalessandro@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (47, 30949, N'Panatta', N'Umberto', N'Silver', N'Arbitro Regionale', N'Serie C', N'umbfip@libero.it
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (48, 58037, N'Pastore', N'Luca', N'Gold', N'Arbitro Regionale', N'Serie C', N'pastore.luca98@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (49, 36762, N'Picano', N'Andrea', N'Silver', N'Arbitro Regionale', N'Serie C', N'andpicano@virgilio.it
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (50, 59267, N'Prundaru', N'Dumitru Cosmin', N'Gold', N'Arbitro Regionale', N'Serie C', N'cosminprundaru@hotmail.it
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (51, 63271, N'Quaranta', N'Alessia', N'Silver', N'Arbitro Regionale', N'Serie C', N'alessia.quaranta40@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (52, 46379, N'Rizzuto', N'Roberto', N'Gold', N'Arbitro Regionale', N'Serie C', N'rizzutoroberto94@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (53, 53342, N'Salamone', N'Tommaso Alberto', N'Silver', N'Arbitro Regionale', N'Serie C', N'tommaso.salamone@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (54, 44331, N'Salvatori', N'Simone', N'Gold', N'Arbitro Nazionale', N'Serie C', N'sarva94@yahoo.it
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (55, 53338, N'Simula', N'Alberto', N'Silver', N'Arbitro Regionale', N'Serie C', N'albysix@hotmail.it
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (57, 35561, N'Suriani', N'Andrea', N'Gold', N'Arbitro Nazionale', N'Serie C', N'andrea.suriani@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (58, 24253, N'Taddei', N'Gianluca', N'Silver', N'Arbitro Regionale', N'Serie C', N'ilconte1967@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (59, 49544, N'Tommasi', N'Emanuela', N'Gold', N'Arbitro Regionale', N'Serie C con deroga A1/F
', N'emanuela94tommasi@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (60, 44087, N'Tripi', N'Stefano', N'Gold', N'Arbitro Nazionale', N'Serie C', N'stefanotripi5@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (61, 57996, N'Venditti', N'Leonardo', N'Gold', N'Arbitro Regionale', N'Serie C', N'vendittileonardo@gmail.com
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (62, 54816, N'Verdecchia', N'Alessandro', N'Silver', N'Arbitro Regionale', N'Serie C', N'aleverd1996@libero.it
', 0)
INSERT [dbo].[Arbitro] ([Id], [Tessera], [Cognome], [Nome], [Categoria], [Qualifica], [Abilitazione], [Email], [NPartiteArbitrate]) VALUES (63, 40091, N'Zambotto', N'Alessandro', N'Gold', N'Arbitro Nazionale', N'Serie C', N'alessandrozambotto@libero.it
', 0)
SET IDENTITY_INSERT [dbo].[Arbitro] OFF
GO
SET IDENTITY_INSERT [dbo].[Calendario] ON 

INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (2, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-10-16' AS Date), 16, 17, NULL, NULL, N'Palazzetto Dello Sport', N'Via Luigi Einaudi 7', N'Sabato', N'Frascati', N'Roma', N'20:45', 1)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (4, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-10-16' AS Date), 18, 19, NULL, NULL, N'Arena Altero Felici', N'Via Flaminio 867', N'Sabato', N'Roma', N'Roma', N'18:15', 1)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (5, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-10-16' AS Date), 20, 21, NULL, NULL, N'Palazzetto Claudio Spano', N'Viale S.Paolo 12', N'Sabato', N'Roma', N'Roma', N'18:00', 1)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (7, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-10-16' AS Date), 22, 23, NULL, NULL, N'BK Anzio - C.S. 4 Casette', N'Via Nettunense Km 36.000', N'Sabato', N'Anzio', N'Roma', N'18:30', 1)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (8, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-10-24' AS Date), 23, 16, NULL, NULL, N'Palestra Petriana', N'Via S.Maria Mediatrice 22', N'Domenica', N'Roma', N'Roma', N'18:00', 2)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (9, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-10-24' AS Date), 21, 47, NULL, NULL, N'Palatarquini', N'Via Mura Francesi 174', N'Domenica', N'Ciampino', N'Roma', N'18:30', 2)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (11, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-10-24' AS Date), 17, 20, NULL, NULL, N'Palavirtus', N'Via Liguria 6', N'Domenica', N'Fondi', N'Latina', N'18:30', 2)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (12, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-10-23' AS Date), 19, 22, NULL, NULL, N'Palareny - ITIS Pacinotti', N'Via Pasquariello 27', N'Sabato', N'Roma', N'Roma', N'18:30', 2)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (13, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-10-30' AS Date), 47, 17, NULL, NULL, N'TensoStruttura ', N'Via Oppido Mamertina', N'Sabato', N'Roma', N'Roma', N'18:45', 3)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (14, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-10-30' AS Date), 20, 23, NULL, NULL, N'Palazzetto Claudio Spano', N'Viale S.Paolo 12', N'Sabato', N'Roma', N'Roma', N'18:00', 3)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (16, N'A ', N'Serie C', N'Gold', N'Andata', CAST(N'2021-10-30' AS Date), 22, 18, NULL, NULL, N'BK Anzio - C.S. 4 Casette', N'Via Nettunense Km 36.000', N'Sabato', N'Anzio', N'Roma', N'18:30', 3)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (17, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-10-30' AS Date), 16, 19, NULL, NULL, N'Palazzetto Dello Sport', N'Via Luigi Einaudi 7', N'Sabato', N'Frascati', N'Roma', N'20:45', 3)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (18, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-11-06' AS Date), 18, 16, NULL, NULL, N'Arena Altero Felici', N'Via Flaminia 867', N'Sabato', N'Roma', N'Roma', N'18:15', 4)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (19, N'A ', N'Serie C', N'Gold', N'Andata', CAST(N'2021-11-07' AS Date), 17, 21, NULL, NULL, N'Palavirtus', N'Via Liguria 6', N'Domenica', N'Fondi', N'Latina', N'18:30', 4)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (20, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-11-07' AS Date), 23, 47, NULL, NULL, N'Palestra Petriana', N'Via S.Maria Mediatrice 22', N'Domenica', N'Roma', N'Roma', N'18:00', 4)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (21, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-11-06' AS Date), 19, 20, NULL, NULL, N'Palareny - ITIS Pacinotti', N'Via Pasquariello 27', N'Sabato', N'Roma', N'Roma', N'18:30', 4)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (22, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-11-13' AS Date), 20, 18, NULL, NULL, N'Palazzetto Claudio Spano', N'Viale S.Paolo 12', N'Sabato', N'Roma', N'Roma', N'18:00', 5)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (23, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-11-14' AS Date), 21, 23, NULL, NULL, N'Palatarquini', N'Via Mura Francesi 174', N'Domenica', N'Ciampino', N'Roma', N'18:30', 5)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (24, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-11-13' AS Date), 16, 22, NULL, NULL, N'Palazzetto Dello Sport', N'Via Luigi Einaudi 7', N'Sabato', N'Frascati', N'Roma', N'20:45', 5)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (25, N'A', N'serie C', N'Gold', N'Andata', CAST(N'2021-11-13' AS Date), 47, 19, NULL, NULL, N'TensoStruttura ', N'Via Oppido Mamertina', N'Sabato', N'Roma', N'Roma', N'18:45', 5)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (26, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-11-21' AS Date), 23, 17, NULL, NULL, N'Palestra Petriana', N'Via S.Maria Mediatrice 22', N'Domenica', N'Roma', N'Roma', N'18:00', 6)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (27, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-11-20' AS Date), 19, 21, NULL, NULL, N'Palareny - ITIS Pacinotti', N'Via Pasquariello 27', N'Sabato', N'Roma', N'Roma', N'18:30', 6)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (28, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-11-20' AS Date), 22, 20, NULL, NULL, N'BK Anzio - C.S. 4 Casette', N'Via Nettunense Km 36.000', N'Sabato', N'Anzio', N'Roma', N'18:30', 6)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (30, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-11-20' AS Date), 18, 47, NULL, NULL, N'Arena Altero Felici', N'Via Flaminia 867', N'Sabato', N'Roma', N'Roma', N'18:15', 6)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (31, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-11-27' AS Date), 20, 16, NULL, NULL, N'Palazzetto Claudio Spano', N'Viale S.Paolo 12', N'Sabato', N'Roma', N'Roma', N'18:00', 7)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (32, N'A ', N'Serie C', N'Gold', N'Andata', CAST(N'2021-11-28' AS Date), 21, 18, NULL, NULL, N'Palatarquini', N'Via Mura Francesi 174', N'Domenica', N'Ciampino', N'Roma', N'18:30', 7)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (33, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-11-27' AS Date), 47, 22, NULL, NULL, N'TensoStruttura ', N'Via Oppido Mamertina', N'Sabato', N'Roma', N'Roma', N'18:45', 7)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (34, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-11-28' AS Date), 17, 19, NULL, NULL, N'Palavirtus', N'Via Liguria 6', N'Domenica', N'Fondi', N'Latina', N'18:30', 7)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (35, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-12-04' AS Date), 16, 47, NULL, NULL, N'Palazzetto Dello Sport', N'Via Luigi Einaudi 7', N'Sabato', N'Frascati', N'Roma', N'20:45', 8)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (36, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-12-04' AS Date), 18, 17, NULL, NULL, N'Arena Altero Felici', N'Via Flaminia 867', N'Sabato', N'Roma', N'Roma', N'18:15', 8)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (37, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-12-04' AS Date), 22, 21, NULL, NULL, N'BK Anzio - C.S. 4 Casette', N'Via Nettunense Km 36.000', N'Sabato', N'Anzio', N'Roma', N'18:30', 8)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (38, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-12-04' AS Date), 19, 23, NULL, NULL, N'Palareny - ITIS Pacinotti', N'Via Pasquariello 27', N'Sabato', N'Roma', N'Roma', N'18:30', 8)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (39, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-12-04' AS Date), 23, 18, NULL, NULL, N'Palestra Petriana', N'Via S.Maria Mediatrice 22', N'Domenica', N'Roma', N'Roma', N'18:00', 9)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (40, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-12-12' AS Date), 17, 22, NULL, NULL, N'Palavirtus', N'Via Liguria 6', N'Domenica', N'Fondi', N'Latina', N'18:30', 9)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (41, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-12-11' AS Date), 47, 20, NULL, NULL, N'TensoStruttura ', N'Via Oppido Mamertina', N'Sabato', N'Roma', N'Roma', N'18:45', 9)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (42, N'A', N'Serie C', N'Gold', N'Andata', CAST(N'2021-12-12' AS Date), 21, 16, NULL, NULL, N'Palatarquini', N'Via Mura Francesi 174', N'Domenica', N'Ciampino', N'Roma', N'18:30', 9)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (43, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2021-12-18' AS Date), 19, 18, NULL, NULL, N'Palareny - ITIS Pacinotti', N'Via Pasquariello 27', N'Sabato', N'Roma', N'Roma', N'18:30', 1)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (44, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2021-12-19' AS Date), 23, 22, NULL, NULL, N'Palestra Petriana', N'Via S.Maria Mediatrice 22', N'Domenica', N'Roma', N'Roma', N'18:00', 1)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (45, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2021-12-19' AS Date), 17, 16, NULL, NULL, N'Palavirtus', N'Via Liguria 6', N'Domenica', N'Fondi', N'Latina', N'18:30', 1)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (46, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2021-12-19' AS Date), 21, 20, NULL, NULL, N'Palatarquini', N'Via Mura Francesi 174', N'Domenica', N'Ciampino', N'Roma', N'18:30', 1)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (47, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-01-08' AS Date), 16, 23, NULL, NULL, N'Palazzetto Dello Sport', N'Via Luigi Einaudi 7', N'Sabato', N'Frascati', N'Roma', N'20:45', 2)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (48, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-01-08' AS Date), 47, 21, NULL, NULL, N'TensoStruttura ', N'Via Oppido Mamertina', N'Sabato', N'Roma', N'Roma', N'18:45', 2)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (49, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-01-08' AS Date), 22, 19, NULL, NULL, N'BK Anzio - C.S. 4 Casette', N'Via Nettunense Km 36.000', N'Sabato', N'Anzio', N'Roma', N'18:30', 2)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (50, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-01-08' AS Date), 20, 17, NULL, NULL, N'Palazzetto Claudio Spano', N'Viale S.Paolo 12', N'Sabato', N'Roma', N'Roma', N'18:00', 2)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (51, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-01-15' AS Date), 18, 22, NULL, NULL, N'Arena Altero Felici', N'Via Flaminia 867', N'Sabato', N'Roma', N'Roma', N'18:15', 3)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (52, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-01-15' AS Date), 19, 16, NULL, NULL, N'Palareny - ITIS Pacinotti', N'Via Pasquariello 27', N'Sabato', N'Roma', N'Roma', N'18:30', 3)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (53, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-01-16' AS Date), 17, 47, NULL, NULL, N'Palavirtus', N'Via Liguria 6', N'Domenica', N'Fondi', N'Latina', N'18:30', 3)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (55, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-01-16' AS Date), 23, 20, NULL, NULL, N'Palestra Petriana', N'Via S.Maria Mediatrice 22', N'Domenica', N'Roma', N'Roma', N'18:00', 3)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (56, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-01-22' AS Date), 47, 23, NULL, NULL, N'TensoStruttura ', N'Via Oppido Mamertina', N'Sabato', N'Roma', N'Roma', N'18:45', 4)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (57, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-01-22' AS Date), 16, 18, NULL, NULL, N'Palazzetto Dello Sport', N'Via Luigi Einaudi 7', N'Sabato', N'Frascati', N'Roma', N'20:45', 4)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (59, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-01-22' AS Date), 20, 19, NULL, NULL, N'Palazzetto Claudio Spano', N'Viale S.Paolo 12', N'Sabato', N'Roma', N'Roma', N'18:00', 4)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (60, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-01-23' AS Date), 21, 17, NULL, NULL, N'Palatarquini', N'Via Mura Francesi 174', N'Domenica', N'Ciampino', N'Roma', N'18:30', 4)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (63, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-01-29' AS Date), 18, 20, NULL, NULL, N'Arena Altero Felici', N'Via Flaminia 867', N'Sabato', N'Roma', N'Roma', N'18:15', 5)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (65, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-01-29' AS Date), 19, 47, NULL, NULL, N'Palareny - ITIS Pacinotti', N'Via Pasquariello 27', N'Sabato', N'Roma', N'Roma', N'18:30', 5)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (66, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-01-30' AS Date), 23, 21, NULL, NULL, N'Palestra Petriana', N'Via S.Maria Mediatrice 22', N'Domenica', N'Roma', N'Roma', N'18:00', 5)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (67, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-01-29' AS Date), 22, 16, NULL, NULL, N'BK Anzio - C.S. 4 Casette', N'Via Nettunense Km 36.000', N'Sabato', N'Anzio', N'Roma', N'18:30', 5)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (69, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-02-05' AS Date), 47, 18, NULL, NULL, N'TensoStruttura ', N'Via Oppido Mamertina', N'Sabato', N'Roma', N'Roma', N'18:45', 6)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (71, N'A ', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-02-06' AS Date), 21, 19, NULL, NULL, N'Palatarquini', N'Via Mura Francesi 174', N'Domenica', N'Ciampino', N'Roma', N'18:30', 6)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (72, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-02-06' AS Date), 17, 23, NULL, NULL, N'Palavirtus', N'Via Liguria 6', N'Domenica', N'Fondi', N'Latina', N'18:30', 6)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (73, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-02-05' AS Date), 20, 22, NULL, NULL, N'Palazzetto Claudio Spano', N'Viale S.Paolo 12', N'Sabato', N'Roma', N'Roma', N'18:00', 6)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (74, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-02-12' AS Date), 18, 21, NULL, NULL, N'Arena Altero Felici', N'Via Flaminia 867', N'Sabato', N'Roma', N'Roma', N'18:15', 7)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (75, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-02-12' AS Date), 22, 47, NULL, NULL, N'BK Anzio - C.S. 4 Casette', N'Via Nettunense Km 36.000', N'Sabato', N'Anzio', N'Roma', N'18:30', 7)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (76, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-02-12' AS Date), 19, 17, NULL, NULL, N'Palareny - ITIS Pacinotti', N'Via Pasquariello 27', N'Sabato', N'Roma', N'Roma', N'18:30', 7)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (80, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-02-12' AS Date), 16, 20, NULL, NULL, N'Palazzetto Dello Sport', N'Via Luigi Einaudi 7', N'Sabato', N'Roma', N'Frascati', N'20:45', 7)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (81, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-02-20' AS Date), 21, 22, NULL, NULL, N'Palatarquini', N'Via Mura Francesi 174', N'Domenica', N'Roma', N'Ciampino', N'18:30', 8)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (82, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-02-20' AS Date), 17, 18, NULL, NULL, N'Palavirtus', N'Via Liguria 6', N'Domenica', N'Latina', N'Fondi', N'18:30', 8)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (83, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-02-20' AS Date), 23, 19, NULL, NULL, N'Palestra Petriana', N'Via S.Maria Mediatrice 22', N'Domenica', N'Roma', N'Roma', N'18:00', 8)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (85, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-02-19' AS Date), 47, 16, NULL, NULL, N'TensoStruttura ', N'Via Oppido Mamertina', N'Sabato', N'Roma', N'Roma', N'18:45', 8)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (86, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-02-26' AS Date), 18, 23, NULL, NULL, N'Arena Altero Felici', N'Via Flaminia 867', N'Sabato', N'Roma', N'Roma', N'18:30', 9)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (87, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-02-26' AS Date), 20, 47, NULL, NULL, N'Palazzetto Claudio Spano', N'Viale S.Paolo 12', N'Sabato', N'Roma', N'Roma', N'18:30', 9)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (88, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-02-26' AS Date), 16, 21, NULL, NULL, N'Palazzetto Dello Sport', N'Via Luigi Einaudi 7', N'Sabato', N'Roma', N'Frascati', N'18:30', 9)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (91, N'A', N'Serie C', N'Gold', N'Ritorno', CAST(N'2022-02-26' AS Date), 22, 17, NULL, NULL, N'BK Anzio - C.S. 4 Casette', N'Via Nettunense Km 36.000', N'Sabato', N'Roma', N'Anzio', N'18:30', 9)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (94, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-10-10' AS Date), 32, 33, NULL, NULL, N'Palatarquini', N'Via Mura Francesi, 174', N'Domenica', N'Roma', N'Ciampino', N'18:30', 1)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (96, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-10-10' AS Date), 35, 36, NULL, NULL, N'Palaassobalneari', N'Piazza Regina Pacis 13', N'Domenica', N'Roma', N'Roma', N'18:30', 1)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (97, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-10-09' AS Date), 37, 38, NULL, NULL, N'Palestra', N'Via XXV Aprile- Loc. La Rosta', N'Sabato', N'Roma', N'Riano', N'18:00', 1)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (98, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-10-09' AS Date), 39, 40, NULL, NULL, N'Palafonte(Arena Superiore)', N'Via R.Ferruzzi 112', N'Sabato', N'Roma', N'Roma', N'18:45', 1)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (99, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-10-10' AS Date), 41, 42, NULL, NULL, N'Pallone Tensostatico', N'Via Respighi S.N.C', N'Domenica', N'Latina', N'Aprilia', N'18:00', 1)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (101, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-10-09' AS Date), 43, 44, NULL, NULL, N'Palasport Città di Frosinone', N'Via Casaleno', N'Sabato', N'Frosinone', N'Frosinone', N'18:30', 1)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (102, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-10-09' AS Date), 45, 46, NULL, NULL, N'Palacerioli', N'Via Filippo Tajani 50', N'Sabato', N'Roma', N'Roma', N'18:00', 1)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (103, N'B', N'Serie C', N'Silver ', N'Andata', CAST(N'2021-10-17' AS Date), 33, 35, NULL, NULL, N'Palestra Itis G.Giorgi', N'Via G.Perlasca', N'Domenica', N'Roma', N'Roma', N'18:15', 2)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (104, N'B', N'Serie C', N'Silver ', N'Andata', CAST(N'2021-10-17' AS Date), 44, 37, NULL, NULL, N'Sporting Club', N'Via San Bartolomeo SNC', N'Domenica', N'Roma', N'Cassino', N'18:00', 2)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (105, N'B', N'Serie', N'Silver', N'Andata', CAST(N'2021-10-16' AS Date), 38, 32, NULL, NULL, N'Scuola Elementare Barchiesi', N'Vicolo Schiavi', N'Sabato', N'Roma', N'Colleferro', N'18:30', 2)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (110, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2010-10-17' AS Date), 36, 45, NULL, NULL, N'Pal.To Sport', N'Via C.Colombo', N'Domenica', N'Latina', N'Scauri', N'18:00', 2)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (112, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-10-17' AS Date), 40, 43, NULL, NULL, N'C.S Cellulosa E.Antonelli', N'Via della Cellulosa 29', N'Domenica', N'Roma', N'Roma', N'20:30', 2)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (115, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-10-16' AS Date), 46, 41, NULL, NULL, N'Palestra Comunale', N'Via Vascarelle 110', N'Sabato', N'Roma', N'Albano Laz', N'19:30', 2)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (116, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-10-17' AS Date), 42, 39, NULL, NULL, N'Pala Margherita Hack', N'Via Singen SNC', N'Domenica', N'Roma', N'Pomezia', N'18:30', 2)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (117, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-10-23' AS Date), 45, 41, NULL, NULL, N'Palacerioli', N'Via Filippo Tajani 50', N'Sabato', N'Roma', N'Roma', N'18:00', 3)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (118, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-10-24' AS Date), 36, 33, NULL, NULL, N'Pal.To Sport', N'Via C.Colombo', N'Domenica', N'Latina', N'Scauri', N'18:00', 3)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (119, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-10-23' AS Date), 39, 46, NULL, NULL, N'Palafonte(Arena Superiore)', N'Via R.Ferruzzi 112', N'Sabato', N'Roma', N'Roma', N'18:45', 3)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (120, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-10-23' AS Date), 37, 40, NULL, NULL, N'Palestra', N'Via XXV Aprile- Loc. La Rosta', N'Sabato', N'Roma', N'Riano', N'18:00', 3)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (121, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-10-23' AS Date), 43, 42, NULL, NULL, N'Palasport Città di Frosinone', N'Via Casaleno', N'Sabato', N'Frosinone', N'Frosinone', N'18:30', 3)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (122, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-10-24' AS Date), 32, 44, NULL, NULL, N'Palatarquini', N'Via Mura Francesi, 174', N'Domenica', N'Roma', N'Ciampino', N'18:30', 3)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (123, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-10-24' AS Date), 35, 38, NULL, NULL, N'Palaassobalneari', N'Piazza Regina Pacis 13', N'Domenica', N'Roma', N'Roma', N'18:30', 3)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (124, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-10-31' AS Date), 41, 39, NULL, NULL, N'Pallone Tensostatico', N'Via Respighi S.N.C', N'Domenica', N'Latina', N'Aprilia', N'18:00', 4)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (125, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-10-31' AS Date), 42, 37, NULL, NULL, N'Pala Margherita Hack', N'Via Singen SNC', N'Domenica', N'Roma', N'Pomezia', N'18:30', 4)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (126, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-10-31' AS Date), 33, 45, NULL, NULL, N'Palestra Itis G.Giorgi', N'Via G.Perlasca', N'Domenica', N'Roma', N'Roma', N'18:15', 4)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (127, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-10-31' AS Date), 40, 32, NULL, NULL, N'C.S Cellulosa E.Antonelli', N'Via della Cellulosa 29', N'Domenica', N'Roma', N'Roma', N'20:30', 4)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (128, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-10-30' AS Date), 46, 43, NULL, NULL, N'Palestra Comunale', N'Via Vascarelle 110', N'Sabato', N'Roma', N'Albano Laz', N'19:30', 4)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (129, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-10-30' AS Date), 38, 36, NULL, NULL, N'Scuola Elementare Barchiesi', N'Vicolo Schiavi', N'Sabato', N'Roma', N'Colleferro', N'18:30', 4)
GO
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (130, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-10-31' AS Date), 44, 35, NULL, NULL, N'Sporting Club', N'Via San Bartolomeo SNC', N'Domenica', N'Frosinone', N'Cassino', N'18:00', 4)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (131, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-07' AS Date), 33, 38, NULL, NULL, N'Palestra Itis G.Giorgi', N'Via G.Perlasca', N'Domenica', N'Roma', N'Roma', N'18:15', 5)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (132, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-07' AS Date), 35, 40, NULL, NULL, N'Palaassobalneari', N'Piazza Regina Pacis 13', N'Domenica', N'Roma', N'Roma', N'18:30', 5)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (133, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-06' AS Date), 37, 46, NULL, NULL, N'Palestra', N'Via XXV Aprile- Loc. La Rosta', N'Sabato', N'Roma', N'Riano', N'18:00', 5)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (134, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-07' AS Date), 36, 44, NULL, NULL, N'Pal.To Sport', N'Via C.Colombo', N'Domenica', N'Latina', N'Scauri', N'18:00', 5)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (135, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-06' AS Date), 43, 41, NULL, NULL, N'Palasport Città di Frosinone', N'Via Casaleno', N'Sabato', N'Frosinone', N'Frosinone', N'18:30', 5)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (136, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-07' AS Date), 32, 42, NULL, NULL, N'Palatarquini', N'Via Mura Francesi, 174', N'Domenica', N'Roma', N'Ciampino', N'18:30', 5)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (137, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-06' AS Date), 45, 39, NULL, NULL, N'Palacerioli', N'Via Filippo Tajani 50', N'Sabato', N'Roma', N'Roma', N'18:00', 5)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (138, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-14' AS Date), 41, 37, NULL, NULL, N'Pallone Tensostatico', N'Via Respighi S.N.C', N'Domenica', N'Latina', N'Aprilia', N'18:00', 6)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (140, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-13' AS Date), 46, 32, NULL, NULL, N'Palestra Comunale', N'Via Vascarelle 110', N'Sabato', N'Roma', N'Albano Laz', N'19:30', 6)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (141, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-14' AS Date), 44, 33, NULL, NULL, N'Sporting Club', N'Via San Bartolomeo SNC', N'Domenica', N'Frosinone', N'Cassino', N'18:00', 6)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (142, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-14' AS Date), 42, 35, NULL, NULL, N'Pala Margherita Hack', N'Via Singen SNC', N'Domenica', N'Roma', N'Pomezia', N'18:30', 6)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (143, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-13' AS Date), 39, 43, NULL, NULL, N'Palafonte(Arena Superiore)', N'Via R.Ferruzzi 112', N'Sabato', N'Roma', N'Roma', N'18:45', 6)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (144, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-13' AS Date), 38, 45, NULL, NULL, N'Scuola Elementare Barchiesi', N'Vicolo Schiavi', N'Sabato', N'Roma', N'Colleferro', N'18:30', 6)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (145, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-14' AS Date), 40, 36, NULL, NULL, N'C.S Cellulosa E.Antonelli', N'Via della Cellulosa 29', N'Domenica', N'Roma', N'Roma', N'20:30', 6)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (146, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-21' AS Date), 36, 42, NULL, NULL, N'Pal.To Sport', N'Via C.Colombo', N'Domenica', N'Latina', N'Scauri', N'18:00', 7)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (147, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-21' AS Date), 33, 40, NULL, NULL, N'Palestra Itis G.Giorgi', N'Via G.Perlasca', N'Domenica', N'Roma', N'Roma', N'18:15', 7)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (148, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-21' AS Date), 32, 41, NULL, NULL, N'Palatarquini', N'Via Mura Francesi, 174', N'Domenica', N'Roma', N'Ciampino', N'18:30', 7)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (149, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-21' AS Date), 35, 46, NULL, NULL, N'Palaassobalneari', N'Piazza Regina Pacis 13', N'Domenica', N'Roma', N'Roma', N'18:30', 7)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (150, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-20' AS Date), 37, 39, NULL, NULL, N'Palestra', N'Via XXV Aprile- Loc. La Rosta', N'Sabato', N'Roma', N'Riano', N'18:00', 7)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (151, N'B', N'Serie C', N'Silver ', N'Andata', CAST(N'2021-11-20' AS Date), 38, 44, NULL, NULL, N'Scuola Elementare Barchiesi', N'Vicolo Schiavi', N'Sabato', N'Roma', N'Colleferro', N'18:30', 7)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (152, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-20' AS Date), 45, 43, NULL, NULL, N'Palacerioli', N'Via Filippo Tajani 50', N'Sabato', N'Roma', N'Roma', N'18:00', 7)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (153, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-27' AS Date), 39, 32, NULL, NULL, N'Palafonte(Arena Superiore)', N'Via R.Ferruzzi 112', N'Sabato', N'Roma', N'Roma', N'18:45', 8)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (154, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-28' AS Date), 40, 38, NULL, NULL, N'C.S Cellulosa E.Antonelli', N'Via della Cellulosa 29', N'Domenica', N'Roma', N'Roma', N'20:30', 8)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (155, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-28' AS Date), 44, 45, NULL, NULL, N'Sporting Club', N'Via San Bartolomeo SNC', N'Domenica', N'Frosinone', N'Cassino', N'18:00', 8)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (156, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-28' AS Date), 42, 33, NULL, NULL, N'Pala Margherita Hack', N'Via Singen SNC', N'Domenica', N'Roma', N'Pomezia', N'18:30', 8)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (157, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-27' AS Date), 43, 37, NULL, NULL, N'Palasport Città di Frosinone', N'Via Casaleno', N'Sabato', N'Frosinone', N'Frosinone', N'18:30', 8)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (158, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-27' AS Date), 46, 36, NULL, NULL, N'Palestra Comunale', N'Via Vascarelle 110', N'Sabato', N'Roma', N'Albano Laz', N'19:30', 8)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (159, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-11-28' AS Date), 41, 35, NULL, NULL, N'Pallone Tensostatico', N'Via Respighi S.N.C', N'Domenica', N'Latina', N'Aprilia', N'18:00', 8)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (160, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-12-04' AS Date), 38, 42, NULL, NULL, N'Scuola Elementare Barchiesi', N'Vicolo Schiavi', N'Sabato', N'Roma', N'Colleferro', N'18:30', 9)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (161, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-12-05' AS Date), 36, 41, NULL, NULL, N'Pal.To Sport', N'Via C.Colombo', N'Domenica', N'Latina', N'Scauri', N'18:00', 9)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (162, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-12-04' AS Date), 45, 37, NULL, NULL, N'Palacerioli', N'Via Filippo Tajani 50', N'Sabato', N'Roma', N'Roma', N'18:00', 9)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (164, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-12-05' AS Date), 33, 46, NULL, NULL, N'Palestra Itis G.Giorgi', N'Via G.Perlasca', N'Domenica', N'Roma', N'Roma', N'18:15', 9)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (165, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-12-05' AS Date), 44, 40, NULL, NULL, N'Sporting Club', N'Via San Bartolomeo SNC', N'Domenica', N'Frosinone', N'Cassino', N'18:00', 9)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (166, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-12-05' AS Date), 35, 39, NULL, NULL, N'Palaassobalneari', N'Piazza Regina Pacis 13', N'Domenica', N'Roma', N'Roma', N'18:30', 9)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (167, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-12-05' AS Date), 32, 43, NULL, NULL, N'Palatarquini', N'Via Mura Francesi, 174', N'Domenica', N'Roma', N'Ciampino', N'18:30', 9)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (168, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-12-11' AS Date), 37, 32, NULL, NULL, N'Palestra', N'Via XXV Aprile- Loc. La Rosta', N'Sabato', N'Roma', N'Riano', N'18:00', 10)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (169, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-12-11' AS Date), 46, 38, NULL, NULL, N'Palestra Comunale', N'Via Vascarelle 110', N'Sabato', N'Roma', N'Albano Laz', N'19:30', 10)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (170, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-12-11' AS Date), 43, 35, NULL, NULL, N'Palasport Città di Frosinone', N'Via Casaleno', N'Sabato', N'Frosinone', N'Frosinone', N'18:30', 10)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (171, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-12-11' AS Date), 39, 36, NULL, NULL, N'Palafonte(Arena Superiore)', N'Via R.Ferruzzi 112', N'Sabato', N'Roma', N'Roma', N'18:45', 10)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (172, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-12-12' AS Date), 42, 44, NULL, NULL, N'Pala Margherita Hack', N'Via Singen SNC', N'Domenica', N'Roma', N'Pomezia', N'18:30', 10)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (173, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-12-12' AS Date), 41, 33, NULL, NULL, N'Pallone Tensostatico', N'Via Respighi S.N.C', N'Domenica', N'Latina', N'Aprilia', N'18:00', 10)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (174, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-12-12' AS Date), 40, 45, NULL, NULL, N'C.S Cellulosa E.Antonelli', N'Via della Cellulosa 29', N'Domenica', N'Roma', N'Roma', N'20:30', 10)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (175, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-12-19' AS Date), 44, 46, NULL, NULL, N'Sporting Club', N'Via San Bartolomeo SNC', N'Domenica', N'Frosinone', N'Cassino', N'18:00', 11)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (176, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-12-19' AS Date), 36, 43, NULL, NULL, N'Pal.To Sport', N'Via C.Colombo', N'Domenica', N'Latina', N'Scauri', N'18:00', 11)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (177, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-12-18' AS Date), 45, 32, NULL, NULL, N'Palacerioli', N'Via Filippo Tajani 50', N'Sabato', N'Roma', N'Roma', N'18:00', 11)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (178, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-12-19' AS Date), 40, 42, NULL, NULL, N'C.S Cellulosa E.Antonelli', N'Via della Cellulosa 29', N'Domenica', N'Roma', N'Roma', N'20:30', 11)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (179, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-12-19' AS Date), 33, 39, NULL, NULL, N'Palestra Itis G.Giorgi', N'Via G.Perlasca', N'Domenica', N'Roma', N'Roma', N'18:15', 11)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (181, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-12-18' AS Date), 38, 41, NULL, NULL, N'Scuola Elementare Barchiesi', N'Vicolo Schiavi', N'Sabato', N'Roma', N'Colleferro', N'18:30', 11)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (182, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-12-19' AS Date), 35, 37, NULL, NULL, N'Palaassobalneari', N'Piazza Regina Pacis 13', N'Domenica', N'Roma', N'Roma', N'18:30', 11)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (183, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2022-01-08' AS Date), 45, 42, NULL, NULL, N'Palacerioli', N'Via Filippo Tajani 50', N'Sabato', N'Roma', N'Roma', N'18:00', 12)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (184, N'B', N'Serie C', N'SIlver', N'Andata', CAST(N'2022-01-08' AS Date), 39, 38, NULL, NULL, N'Palafonte(Arena Superiore)', N'Via R.Ferruzzi 112', N'Sabato', N'Roma', N'Roma', N'18:45', 12)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (185, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2021-12-09' AS Date), 41, 44, NULL, NULL, N'Pallone Tensostatico', N'Via Respighi S.N.C', N'Domenica', N'Latina', N'Aprilia', N'18:00', 12)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (186, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2022-01-08' AS Date), 46, 40, NULL, NULL, N'Palestra Comunale', N'Via Vascarelle 110', N'Sabato', N'Roma', N'Albano Laz', N'19:30', 12)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (187, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2022-01-08' AS Date), 37, 36, NULL, NULL, N'Palestra', N'Via XXV Aprile- Loc. La Rosta', N'Sabato', N'Roma', N'Riano', N'18:00', 12)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (188, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2022-01-09' AS Date), 32, 35, NULL, NULL, N'Palatarquini', N'Via Mura Francesi, 174', N'Domenica', N'Roma', N'Ciampino', N'18:30', 12)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (189, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2022-01-08' AS Date), 43, 33, NULL, NULL, N'Palasport Città di Frosinone', N'Via Casaleno', N'Sabato', N'Frosinone', N'Frosinone', N'18:30', 12)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (190, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2022-01-15' AS Date), 38, 43, NULL, NULL, N'Scuola Elementare Barchiesi', N'Vicolo Schiavi', N'Sabato', N'Roma', N'Colleferro', N'18:30', 13)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (191, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2022-01-16' AS Date), 42, 46, NULL, NULL, N'Pala Margherita Hack', N'Via Singen SNC', N'Domenica', N'Roma', N'Pomezia', N'18:30', 13)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (192, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2022-01-16' AS Date), 35, 45, NULL, NULL, N'Palaassobalneari', N'Piazza Regina Pacis 13', N'Domenica', N'Roma', N'Roma', N'18:30', 13)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (193, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2022-01-16' AS Date), 40, 41, NULL, NULL, N'C.S Cellulosa E.Antonelli', N'Via della Cellulosa 29', N'Domenica', N'Roma', N'Roma', N'20:30', 13)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (194, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2022-01-16' AS Date), 36, 32, NULL, NULL, N'Pal.To Sport', N'Via C.Colombo', N'Domenica', N'Latina', N'Scauri', N'18:00', 13)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (195, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2022-01-16' AS Date), 33, 37, NULL, NULL, N'Palestra Itis G.Giorgi', N'Via G.Perlasca', N'Domenica', N'Roma', N'Roma', N'18:15', 13)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (196, N'B', N'Serie C', N'Silver', N'Andata', CAST(N'2022-01-16' AS Date), 44, 39, NULL, NULL, N'Sporting Club', N'Via San Bartolomeo SNC', N'Domenica', N'Frosinone', N'Cassino', N'18:00', 13)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (197, N'B', N'Serie C', N'Silver', N'Ritorno', CAST(N'2022-01-23' AS Date), 33, 32, NULL, NULL, N'Palestra Itis G.Giorgi', N'Via G.Perlasca', N'Domenica', N'Roma', N'Roma', N'18:15', 1)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (198, N'B', N'Serie C', N'Silver', N'Ritorno', CAST(N'2022-01-23' AS Date), 42, 41, NULL, NULL, N'Pala Margherita Hack', N'Via Singen SNC', N'Domenica', N'Roma', N'Pomezia', N'18:30', 1)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (199, N'B', N'Serie C', N'Silver', N'Ritorno', CAST(N'2022-01-23' AS Date), 36, 35, NULL, NULL, N'Pal.To Sport', N'Via C.Colombo', N'Domenica', N'Latina', N'Scauri', N'18:00', 1)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (200, N'B', N'Serie C', N'Silver', N'Ritorno', CAST(N'2022-01-23' AS Date), 40, 39, NULL, NULL, N'C.S Cellulosa E.Antonelli', N'Via della Cellulosa 29', N'Domenica', N'Roma', N'Roma', N'20:30', 1)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (201, N'B', N'Serie C', N'Silver', N'Ritorno', CAST(N'2022-01-22' AS Date), 46, 45, NULL, NULL, N'Palestra Comunale', N'Via Vascarelle 110', N'Sabato', N'Roma', N'Albano Laz', N'19:30', 1)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (202, N'B', N'Serie C', N'Silver', N'Ritorno', CAST(N'2022-01-23' AS Date), 44, 43, NULL, NULL, N'Sporting Club', N'Via San Bartolomeo SNC', N'Domenica', N'Frosinone', N'Cassino', N'18:00', 1)
INSERT [dbo].[Calendario] ([Id], [Girone], [Campionato], [Categoria], [TipoGiornata], [Data], [SquadraCasaId], [SquadraOspiteId], [Arbitro1Id], [Arbitro2Id], [Campo], [Indirizzo], [GiornoPartita], [Citta], [Provincia], [Ora], [Giornata]) VALUES (203, N'B', N'Serie C', N'Silver', N'Ritorno', CAST(N'2022-01-22' AS Date), 38, 37, NULL, NULL, N'Scuola Elementare Barchiesi', N'Vicolo Schiavi', N'Sabato', N'Roma', N'Colleferro', N'18:30', 1)
SET IDENTITY_INSERT [dbo].[Calendario] OFF
GO
SET IDENTITY_INSERT [dbo].[Squadra] ON 

INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (1, N'New Basket Time', N'A', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (2, N'Scuola Basket Roma', N'A', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (4, N'Pallacanestro Sora', N'A', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (5, N'Algarve Roma Torrino', N'A', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (6, N'Lasalle', N'A', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (7, N'Basket San Cesareo', N'A', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (8, N'Basket Serapo 85', N'A', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (9, N'Vis Nova Basket', N'A', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (10, N'Palocco', N'A', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (11, N'Virtus Velletri', N'A', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (12, N'Basket Roma Asd', N'A', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (13, N'Basket Ferentino 1977', N'A', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (14, N'Roma Eur', N'A', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (15, N'Fortitudo Anagni MMX', N'A', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (16, N'Club Basket Frascati', N'A', N'Gold')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (17, N'Virtus Basket Fondi', N'A', N'Gold')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (18, N'Stella Azzurra', N'A', N'Gold')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (19, N'Lazio Basketball ATG', N'A', N'Gold')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (20, N'San Paolo Ostiense', N'A', N'Gold')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (21, N'P.G.Frassati', N'A', N'Gold')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (22, N'Anzio Basket Club', N'A', N'Gold')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (23, N'Virtus Roma 1960', N'A', N'Gold')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (24, N'Smit Roma Centro', N'B', N'Gold')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (25, N'Alfa Omega', N'B', N'Gold')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (26, N'Cest.Civitavecchia', N'B', N'Gold')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (27, N'Pall.Stella Azzurra VT', N'B', N'Gold')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (28, N'Tiber Basket', N'B', N'Gold')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (29, N'San Nilo Grottaferrata', N'B', N'Gold')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (30, N'Pallacanestro Palestrina', N'B ', N'Gold')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (31, N'Virtus Valmontone', N'B', N'Gold')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (32, N'Nova Basket Ciampino', N'B', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (33, N'PGS Borgo Don Bosco', N'B', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (35, N'Stelle Marine', N'B', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (36, N'Fortitudo Scauri', N'B', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (37, N'Nuova Lazio Pall.', N'B', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (38, N'Pall.Colleferro', N'B', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (39, N'Fonte Roma Basket', N'B', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (40, N'Uisp XVIII', N'B', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (41, N'Virtus Basket Aprilia', N'B ', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (42, N'Virtus Settanta Pomezia ', N'B', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (43, N'Scuola Basket Frosinone', N'B', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (44, N'Basket Cassino', N'B', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (45, N'Pol.Cult.Vigna Pia', N'B', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (46, N'Albano Basket Club', N'B ', N'Silver')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (47, N'Carver Cinecitta', N'A', N'Gold')
INSERT [dbo].[Squadra] ([Id], [NomeSquadra], [Girone], [Categoria]) VALUES (48, N'Pass Roma', N'B', N'Gold')
SET IDENTITY_INSERT [dbo].[Squadra] OFF
GO
SET IDENTITY_INSERT [dbo].[Utente] ON 

INSERT [dbo].[Utente] ([Id], [Nome], [Cognome], [Email], [Password], [Ruolo]) VALUES (1, N'Matteo', N'Fuccelli', N'aram@gmail.com', N'12345', N'ADMIN')
INSERT [dbo].[Utente] ([Id], [Nome], [Cognome], [Email], [Password], [Ruolo]) VALUES (2, N'Ele', N'Fuccia', N'ele@gmail.com', N'12345', N'ADMIN')
INSERT [dbo].[Utente] ([Id], [Nome], [Cognome], [Email], [Password], [Ruolo]) VALUES (3, N'Alessia', N'Bevilacqua', N'alessiabevi@gmail.com', N'1234', N'USER')
INSERT [dbo].[Utente] ([Id], [Nome], [Cognome], [Email], [Password], [Ruolo]) VALUES (4, N'Manu', N'Manu', N'manu@gmail.com', N'1234', N'USER')
SET IDENTITY_INSERT [dbo].[Utente] OFF
GO
ALTER TABLE [dbo].[ArbitroEccezione]  WITH CHECK ADD  CONSTRAINT [FK_ArbitroEccezione_Eccezione] FOREIGN KEY([EccezioneId])
REFERENCES [dbo].[Eccezione] ([Id])
GO
ALTER TABLE [dbo].[ArbitroEccezione] CHECK CONSTRAINT [FK_ArbitroEccezione_Eccezione]
GO
ALTER TABLE [dbo].[Eccezione]  WITH CHECK ADD  CONSTRAINT [FK_Eccezione_AnagraficaSquadra] FOREIGN KEY([SquadraId])
REFERENCES [dbo].[Squadra] ([Id])
GO
ALTER TABLE [dbo].[Eccezione] CHECK CONSTRAINT [FK_Eccezione_AnagraficaSquadra]
GO
/****** Object:  StoredProcedure [dbo].[ArbitriRandom]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ArbitriRandom]
	
	@IdPartita int,
	@Squadra1 int = 0,
	@Squadra2 int = 0,
	@Categoria varchar(10) = '',
	@Giornata int = 0,
	@GiornataAttuale int = 0,
	@TipoGiornata varchar(20) = '',
	@MenoPartiteArbitrate int = 0
	

AS
BEGIN
/* controllo categoria*/

IF((SELECT COUNT(0) FROM Calendario WHERE Arbitro1Id != NULL AND Arbitro2Id != NULL AND Id = @IdPartita) = 0)
Begin
	DECLARE @Arb1 int = (SELECT Calendario.Arbitro1Id FROM Calendario WHERE  Id = @IdPartita)
	DECLARE @Arb2 int = (SELECT Calendario.Arbitro2Id FROM Calendario WHERE  Id = @IdPartita)
	UPDATE Arbitro SET NPartiteArbitrate = (NPartiteArbitrate - 1) WHERE Id = @Arb1 OR Id= @Arb2
End

DECLARE @ResultTable TABLE (IdA int, NPartiteArbitrate int, TotSquadra int)

SET @Squadra1 = (SELECT Calendario.SquadraCasaId FROM Calendario WHERE Calendario.Id = @IdPartita)
print @Squadra1

SET @Squadra2 = (SELECT Calendario.SquadraOspiteId FROM Calendario WHERE Calendario.Id = @IdPartita)
print @Squadra2

SET @Categoria = (SELECT Calendario.Categoria FROM Calendario WHERE Calendario.SquadraCasaId = @Squadra1 AND Calendario.SquadraOspiteId = @Squadra2)
print @Categoria;

SET @Giornata = (SELECT Calendario.Giornata FROM Calendario WHERE Calendario.SquadraCasaId = @Squadra1 AND Calendario.SquadraOspiteId = @Squadra2)
print @Giornata;

SET @TipoGiornata = (SELECT Calendario.TipoGiornata FROM Calendario WHERE Calendario.SquadraCasaId = @Squadra1 AND Calendario.SquadraOspiteId = @Squadra2)
print @TipoGiornata;


print '-----------------------------------'

/* controllo arbitri disponibili*/

DECLARE @Nome varchar(50), @Cognome varchar(50), @IdArb int, @CategoriaArbitro varchar(10), @Disponibile int,@Disponibile2 int, @Eccezione int, @NPartiteArbitrate int, @Rand1 int, @Rand2 int;

IF(@Categoria = 'Gold')
Begin
	DECLARE cursor_prova CURSOR FOR
	SELECT Nome, Cognome,Id, Categoria, NPartiteArbitrate FROM Arbitro
	WHERE Categoria = 'Gold'
END
ELSE
Begin
	DECLARE cursor_prova CURSOR FOR
	SELECT Nome, Cognome,Id, Categoria, NPartiteArbitrate FROM Arbitro
End
OPEN cursor_prova;

FETCH NEXT FROM cursor_prova
INTO @Nome, @Cognome, @IdArb, @CategoriaArbitro, @NPartiteArbitrate

WHILE @@FETCH_STATUS = 0
BEGIN 

		IF((SELECT COUNT(0) FROM ArbitroEccezione WHERE ArbitroId = @IdArb) != 0)
	Begin
		EXEC @Disponibile2 = Eccezioni @IdArb, @Squadra1, @Squadra2

		IF(@Disponibile2 = 0)
		Begin
			--INSERT INTO @ResultTable (IdA, NPartiteArbitrate,TotSquadra) 
			--			VALUES (@IdArb, @NPartiteArbitrate, (SELECT COUNT(0) FROM Calendario WHERE (Arbitro1Id = @IdArb OR Arbitro2Id = @IdArb) AND
			--			(SquadraCasaId =  @Squadra1 Or SquadraOspiteId = @Squadra1 OR SquadraCasaId = @Squadra2 Or SquadraOspiteId = @Squadra2)))
		
			EXEC @Disponibile = checkPartiteGiaArbitrate @IdArb, @Squadra1, @Squadra2
			Print @Disponibile
			IF(@Disponibile = 0)
			Begin
			INSERT INTO @ResultTable (IdA, NPartiteArbitrate,TotSquadra) 
						VALUES (@IdArb, @NPartiteArbitrate, (SELECT COUNT(0) FROM Calendario WHERE (Arbitro1Id = @IdArb OR Arbitro2Id = @IdArb) AND
						(SquadraCasaId =  @Squadra1 Or SquadraOspiteId = @Squadra1 OR SquadraCasaId = @Squadra2 Or SquadraOspiteId = @Squadra2)))
			End
		
		End
		End
		ELSE
		Begin
			EXEC @Disponibile = checkPartiteGiaArbitrate @IdArb, @Squadra1, @Squadra2
			Print @Disponibile
			IF(@Disponibile = 0)
			Begin
			INSERT INTO @ResultTable (IdA, NPartiteArbitrate,TotSquadra) 
						VALUES (@IdArb, @NPartiteArbitrate, (SELECT COUNT(0) FROM Calendario WHERE (Arbitro1Id = @IdArb OR Arbitro2Id = @IdArb) AND
						(SquadraCasaId =  @Squadra1 Or SquadraOspiteId = @Squadra1 OR SquadraCasaId = @Squadra2 Or SquadraOspiteId = @Squadra2)))
			End
		End


	
	

		

FETCH NEXT FROM cursor_prova into @Nome, @Cognome, @IdArb, @CategoriaArbitro, @NPartiteArbitrate
END

CLOSE cursor_prova
DEALLOCATE cursor_prova
DECLARE @Np int;
SET @MenoPartiteArbitrate = (SELECT MIN(NPartiteArbitrate) FROM @ResultTable)
SELECT @IdPartita AS ID_Partita
IF((SELECT COUNT(0) FROM @ResultTable WHERE NPartiteArbitrate = @MenoPartiteArbitrate)=0)
Begin

	SET @Np = @MenoPartiteArbitrate + 1
	WHILE (SELECT COUNT(0) FROM @ResultTable WHERE NPartiteArbitrate = @Np)>1
	Begin
		SET @Np = @Np + 1
	End

	SET @Rand1 = (SELECT TOP 1 IdA FROM @ResultTable WHERE NPartiteArbitrate = @Np ORDER BY NEWID())

	SET @Rand2 = (SELECT TOP 1 IdA FROM @ResultTable WHERE NPartiteArbitrate = @Np ORDER BY NEWID())
	
	WHILE @Rand2 = @Rand1
	Begin
		SET @Rand2 = (SELECT TOP 1 IdA FROM @ResultTable WHERE NPartiteArbitrate = @MenoPartiteArbitrate ORDER BY NEWID())
	End

	UPDATE Arbitro SET NPartiteArbitrate = (NPartiteArbitrate + 1) WHERE Id = @Rand1 OR Id=@Rand2

	UPDATE Calendario SET Arbitro1Id = @Rand1, Arbitro2Id = @Rand2 WHERE Id = @IdPartita
END
ELSE IF((SELECT COUNT(0) FROM @ResultTable WHERE NPartiteArbitrate = @MenoPartiteArbitrate)=1)
Begin

	SET @Rand1 = (SELECT IdA FROM @ResultTable WHERE NPartiteArbitrate = @MenoPartiteArbitrate)

	SET @Np = @MenoPartiteArbitrate + 1
	SELECT @Np AS partitePiuUno
	WHILE ((SELECT COUNT(0) FROM @ResultTable WHERE NPartiteArbitrate = @Np)=0)
	Begin
		SET @Np = @Np + 1
	End
	SELECT @Np AS Nuovo_numero_arbritate
	SET @Rand2 = (SELECT TOP 1 IdA FROM @ResultTable WHERE NPartiteArbitrate = @Np ORDER BY NEWID())
	
	WHILE @Rand2 = @Rand1
	Begin
		SET @Rand2 = (SELECT TOP 1 IdA FROM @ResultTable WHERE NPartiteArbitrate = @Np ORDER BY NEWID())
	End

	UPDATE Arbitro SET NPartiteArbitrate = (NPartiteArbitrate + 1) WHERE Id = @Rand1 OR Id=@Rand2

	UPDATE Calendario SET Arbitro1Id = @Rand1, Arbitro2Id = @Rand2 WHERE Id = @IdPartita
	
END

ELSE
Begin
	
	/*Controllo tot volte stessa partita*/
	 
	SET @Rand1 = (SELECT TOP(1) IdA FROM @ResultTable WHERE NPartiteArbitrate = @MenoPartiteArbitrate ORDER BY TotSquadra)

	UPDATE Arbitro SET NPartiteArbitrate = (NPartiteArbitrate + 1) WHERE Id = @Rand1
	UPDATE @ResultTable SET NPartiteArbitrate = (NPartiteArbitrate + 1) WHERE IdA = @Rand1

	SET @Rand2 = (SELECT TOP(1) IdA FROM @ResultTable WHERE NPartiteArbitrate = @MenoPartiteArbitrate ORDER BY TotSquadra)

	UPDATE Arbitro SET NPartiteArbitrate = (NPartiteArbitrate + 1) WHERE Id = @Rand2

	UPDATE Calendario SET Arbitro1Id = @Rand1, Arbitro2Id = @Rand2 WHERE Id = @IdPartita

	/*Fine Controllo tot volte stessa partita*/


	/*SET @Rand1 = (SELECT TOP 1 IdA FROM @ResultTable WHERE NPartiteArbitrate = @MenoPartiteArbitrate ORDER BY NEWID())

	SET @Rand2 = (SELECT TOP 1 IdA FROM @ResultTable WHERE NPartiteArbitrate = @MenoPartiteArbitrate ORDER BY NEWID())*/

END

END
GO
/****** Object:  StoredProcedure [dbo].[checkPartiteGiaArbitrate]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[checkPartiteGiaArbitrate]
	@IdArbitro int,
	@IdSquadraCasa int,
	@IdSquadraOspite int
AS
BEGIN

	DECLARE @GiornataAttuale int,
	 @TipoGiornata varchar(20),
	@AndataRitorno varchar(20),
	@IdPartita int,
	@Data Date;

	SET @TipoGiornata = (SELECT TipoGiornata FROM Calendario WHERE SquadraCasaId = @IdSquadraCasa AND SquadraOspiteId = @IdSquadraOspite)

	SET @GiornataAttuale = (SELECT Giornata FROM Calendario WHERE SquadraCasaId = @IdSquadraCasa AND SquadraOspiteId = @IdSquadraOspite)
	
	SET @AndataRitorno = (SELECT TipoGiornata FROM Calendario WHERE SquadraCasaId = @IdSquadraCasa AND SquadraOspiteId = @IdSquadraOspite)
	
	SET @IdPartita = (SELECT Id FROM Calendario WHERE SquadraCasaId = @IdSquadraCasa AND SquadraOspiteId = @IdSquadraOspite)

	SET @Data =(SELECT Data FROM Calendario WHERE SquadraCasaId = @IdSquadraCasa AND SquadraOspiteId = @IdSquadraOspite)

	/*Partite Inferiori*/

	IF((SELECT COUNT(0) FROM 
		   (SELECT * FROM Calendario WHERE (Giornata = @GiornataAttuale OR Giornata = @GiornataAttuale - 1 OR Giornata = @GiornataAttuale - 2) AND (Id < @IdPartita) AND TipoGiornata = @TipoGiornata) 
		    AS NC WHERE (NC.SquadraCasaId = @IdSquadraOspite OR NC.SquadraOspiteId = @IdSquadraOspite) AND (Arbitro1Id = @IdArbitro OR Arbitro2Id = @IdArbitro)) !=0) 
		Begin
			RETURN 1
		End

	ELSE IF((SELECT COUNT(0) FROM 
		   (SELECT * FROM Calendario WHERE (Giornata = @GiornataAttuale OR Giornata = @GiornataAttuale -1 OR Giornata = @GiornataAttuale -2) AND (Id < @IdPartita) AND TipoGiornata = @TipoGiornata) 
		    AS NC WHERE (NC.SquadraCasaId = @IdSquadraCasa OR NC.SquadraOspiteId = @IdSquadraCasa) AND (Arbitro1Id = @IdArbitro OR Arbitro2Id = @IdArbitro)) !=0) 
		Begin
			RETURN 1
		End

	/*Partite Superiori*/
	IF((SELECT COUNT(0) FROM 
		   (SELECT * FROM Calendario WHERE (Giornata = @GiornataAttuale OR Giornata = @GiornataAttuale + 1 OR Giornata = @GiornataAttuale + 2) AND (Id < @IdPartita) AND TipoGiornata = @TipoGiornata) 
		    AS NC WHERE (NC.SquadraCasaId = @IdSquadraOspite OR NC.SquadraOspiteId = @IdSquadraOspite) AND (Arbitro1Id = @IdArbitro OR Arbitro2Id = @IdArbitro)) !=0) 
		Begin
			RETURN 1
		End

	ELSE IF((SELECT COUNT(0) FROM 
		   (SELECT * FROM Calendario WHERE (Giornata = @GiornataAttuale OR Giornata = @GiornataAttuale +1 OR Giornata = @GiornataAttuale +2) AND (Id < @IdPartita) AND TipoGiornata = @TipoGiornata) 
		    AS NC WHERE (NC.SquadraCasaId = @IdSquadraCasa OR NC.SquadraOspiteId = @IdSquadraCasa) AND (Arbitro1Id = @IdArbitro OR Arbitro2Id = @IdArbitro)) !=0) 
		Begin
			RETURN 1
		End

	/*Conflitto intesse*/
	IF((SELECT COUNT(0) FROM Calendario WHERE (Calendario.Arbitro1Id = @IdArbitro OR Calendario.Arbitro2Id = @IdArbitro)
			 AND (SquadraCasaId = @IdSquadraOspite AND SquadraOspiteId = @IdSquadraCasa))!=0) 
		Begin
			RETURN 1
		End
	
	/*Controllo date*/
	IF((SELECT COUNT(0) FROM Calendario WHERE Data = @Data AND (Arbitro1Id = @IdArbitro OR Arbitro2Id = @IdArbitro))!=0)
	Begin
		Return 1
	End

	/*Controllo arbitraggio stessa giornata*/
	IF((SELECT COUNT(0) FROM Calendario WHERE (Arbitro1Id = @IdArbitro OR Arbitro2Id = @IdArbitro) 
				AND Giornata = (SELECT Giornata From Calendario WHERE SquadraCasaId = @IdSquadraCasa AND SquadraOspiteId = @IdSquadraOspite))!=0)
	Begin
		Return 1
	End
	
	RETURN 0
		
END
GO
/****** Object:  StoredProcedure [dbo].[CicloPartite]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CicloPartite]

	

AS
BEGIN
	
UPDATE Arbitro SET NPartiteArbitrate = 0
UPDATE Calendario SET Arbitro1Id = NULL, Arbitro2Id = NULL

DECLARE @IdPartita int ;

	DECLARE cursor_partite CURSOR FOR
	SELECT Id FROM Calendario

OPEN cursor_partite;

FETCH NEXT FROM cursor_partite
INTO @IdPartita

WHILE @@FETCH_STATUS = 0
BEGIN 

	EXEC ArbitriRandom @IdPartita
		
FETCH NEXT FROM cursor_partite into @IdPartita
END

CLOSE cursor_partite
DEALLOCATE cursor_partite

END
GO
/****** Object:  StoredProcedure [dbo].[deleteArbitro]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[deleteArbitro]

 @Id int

AS
BEGIN
	
	 DELETE FROM Arbitro WHERE Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[deleteCalendario]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[deleteCalendario]

	@Id int

AS
BEGIN
	
	DELETE FROM Calendario WHERE Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[deleteSquadra]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[deleteSquadra]

@Id int

AS
BEGIN
	
	DELETE FROM Squadra WHERE Squadra.Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[Eccezioni]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Eccezioni]
	@IdArbitro int,
	@IdSquadraCasa int,
	@IdSquadraOspite int
AS
BEGIN
	
	/*CONTROLLO DATA INDISPONIBILITA' DELL'ARBITRO PER UNA PARTITA*/

	DECLARE @DataPartita Date, @DataInizio Date, @DataFine Date;

	SET @DataPartita = (SELECT Calendario.Data FROM Calendario WHERE SquadraCasaId = @IdSquadraCasa AND SquadraOspiteId = @IdSquadraOspite)
	Print '@DataPartita= ' + CAST(@DataPartita AS varchar(50)) 
	SET @DataInizio = (SELECT Eccezione.DataInizio FROM Eccezione INNER JOIN ArbitroEccezione
					   ON Eccezione.Id = ArbitroEccezione.EccezioneId
				       WHERE ArbitroEccezione.ArbitroId = @IdArbitro)
	Print '@DataInizio= ' + CAST(@DataInizio AS varchar(50)) 
	SET @DataFine = (SELECT Eccezione.DataFine FROM Eccezione INNER JOIN ArbitroEccezione
					 ON Eccezione.Id = ArbitroEccezione.EccezioneId
				     WHERE ArbitroEccezione.ArbitroId = @IdArbitro)
	Print '@Datafine= ' + CAST(@DataFine AS varchar(50)) 

	Print '@Date diff @dataPartita Data inizio <0' + CAST((DATEDIFF(DD, @DataPartita, @DataInizio)) AS varchar(10))
	Print '@Date diff @dataPartita Data fine >0' + CAST((DATEDIFF(DD, @DataPartita, @DataFine)) AS varchar(10))
	IF(@DataInizio IS NOT NULL )
	Begin
	print 'entrato'
		IF((DATEDIFF(DD, @DataPartita, @DataInizio)<0) AND (DATEDIFF(DD, @DataPartita, @DataFine)>0))
		Begin
			RETURN 1
		End
	End
	IF((SELECT COUNT(0) FROM Eccezione INNER JOIN
	   ArbitroEccezione ON Eccezione.Id = ArbitroEccezione.EccezioneId
       WHERE ArbitroEccezione.ArbitroId = @IdArbitro AND (Eccezione.SquadraId = @IdSquadraOspite OR Eccezione.SquadraId = @IdSquadraCasa))!=0)
	Begin
		RETURN 1
	End



	RETURN 0

END
GO
/****** Object:  StoredProcedure [dbo].[filterArbitro]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[filterArbitro]
	
	@Id int = null,
	@Nome varchar(50)= null,
	@Cognome varchar(50)=null,
	@Tessera int=null,
	@Qualifica varchar(50) =null,
	@Categoria varchar(20) = null,
	@Abilitazione varchar(20) = null,
	@Email nvarchar(50) = null

AS
BEGIN

	IF(@Tessera = 0)
	Begin
		SET @Tessera = null
	End

	IF(@Id = 0)
	Begin
		SET @Id = null
	End
	
	SELECT * FROM Arbitro WHERE Id = isnull(@Id, Id) AND Nome = isnull(@Nome, Nome) AND Cognome = isnull(@Cognome, Cognome)
								AND Tessera = isnull(@Tessera, Tessera) AND Qualifica = isnull(@Qualifica, Qualifica)
								AND Categoria = isnull(@Categoria, Categoria) AND Abilitazione = isnull(@Abilitazione, Abilitazione)
								AND Email = isnull(@Email, Email)
END
GO
/****** Object:  StoredProcedure [dbo].[filterCalendario]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[filterCalendario]

	@Id int = null,
	@Girone varchar(1) = null,
	@Campionato varchar(10) = null,
	@Categoria varchar(10) = null,
	@TipoGiornata varchar(10) = null,
	@Data Date = null,
	@SquadraCasaId int = null,
	@SquadraOspiteId int = null,
	@Arbitro1Id int = null,
	@Giornata int =null,
	@Ora varchar(10) =null

AS
BEGIN
	
	IF(@Id = 0)
	Begin
		SET @Id = NULL
	End

	IF(@SquadraCasaId = 0)
	Begin
		SET @SquadraCasaId = NULL
	End

	IF(@SquadraOspiteId = 0)
	Begin
		SET @SquadraOspiteId = NULL
	End

	IF(@Arbitro1Id = 0)
	Begin
		SET @Arbitro1Id = NULL
	End

	IF(@Giornata = 0)
	Begin
		SET @Giornata = NULL
	End

	IF(DATEDIFF(DD, @Data, '2000-01-01 00:00:00')=0)
	Begin
		SET @Data = NULL
	End

	DECLARE @Select nvarchar(200) = 'SELECT * FROM Calendario WHERE ', @Cont int = 0;

	IF(@Id IS NOT NULL)
	Begin
		SET @Select = @Select + 'Id = ' + CAST(@Id AS varchar(4)) + ' ';
		SET @Cont = 1
	End

	IF(@Categoria IS NOT NULL)
	Begin
		IF(@Cont != 0)
		Begin
			SET @Select = @Select + 'AND ' + 'Categoria = ''' + @Categoria + ''' ';
		End
		ELSE
		Begin
			SET @Select = @Select + 'Categoria = ''' + @Categoria + ''' ';
			SET @Cont = 1
		End
	End

	IF(@Girone IS NOT NULL)
	Begin
		IF(@Cont != 0)
		Begin
			SET @Select = @Select + 'AND ' + 'Girone = ''' + @Girone + ''' ';
		End
		ELSE
		Begin
			SET @Select = @Select + 'Girone = ''' + @Girone + ''' ';
			SET @Cont = 1
		End
	End

	IF(@Campionato IS NOT NULL)
	Begin
		IF(@Cont != 0)
		Begin
			SET @Select = @Select + 'AND ' + 'Campionato =  '''+ @Campionato + ''' ';
		End
		ELSE
		Begin
			SET @Select = @Select + 'Campionato = ''' + @Campionato + ''' ';
			SET @Cont = 1
		End
	End

	IF(@TipoGiornata IS NOT NULL)
	Begin
		IF(@Cont!= 0)
		Begin
			SET @Select = @Select + 'AND ' + 'TipoGiornata = ''' + @TipoGiornata + ''' ';
		End
		ELSE
		Begin
			SET @Select = @Select + 'TipoGiornata = ''' + @TipoGiornata + ''' ';
			SET @Cont = 1
		End
	End
	
	IF(DATEDIFF(DD, @Data, '2000-01-01 00:00:00')=0)
	Begin
		IF(@Cont!= 0)
		Begin
			SET @Select = @Select + 'AND ' + 'Data = ' + CAST(@Data AS varchar(20)) + ' ';
		End
		ELSE
		Begin
			SET @Select = @Select + 'Data = ' + CAST(@Data AS varchar(20)) + ' ';
			SET @Cont = 1
		End
	End

	IF(@SquadraCasaId IS NOT NULL)
	Begin
		IF(@Cont!= 0)
		Begin
			SET @Select = @Select + 'AND ' + 'SquadraCasaId = ' + CAST(@SquadraCasaId AS varchar(60)) + ' ';
		End
		ELSE
		Begin
			SET @Select = @Select + 'SquadraCasaId = ' + CAST(@SquadraCasaId AS varchar(60)) + ' ';
			SET @Cont = 1
		End
	End

	IF(@SquadraOspiteId IS NOT NULL)
	Begin
		IF(@Cont!= 0)
		Begin
			SET @Select = @Select + 'AND ' + 'SquadraOspiteId = ' + CAST(@SquadraOspiteId AS varchar(60)) + ' ';
		End
		ELSE
		Begin
			SET @Select = @Select + 'SquadraOspiteId = ' + CAST(@SquadraOspiteId AS varchar(60)) + ' ';
			SET @Cont = 1
		End
	End

	IF (@Arbitro1Id IS NOT NULL)
	Begin
		IF(@Cont!= 0)
		Begin
			SET @Select = @Select + 'AND ' + 'Arbitro1Id = ' + CAST(@Arbitro1Id AS varchar(60)) + ' OR ' + 'Arbitro2Id = ' + CAST(@Arbitro1Id AS varchar(60)) + ' ' ;
		End
		ELSE
		Begin
			SET @Select = @Select + 'Arbitro1Id = ' + CAST(@Arbitro1Id AS varchar(60)) + ' OR ' + 'Arbitro2Id = ' + CAST(@Arbitro1Id AS varchar(60)) + ' ';
			SET @Cont = 1
		End
	End

	IF(@Giornata IS NOT NULL)
	Begin
		IF(@Cont!= 0)
		Begin
			SET @Select = @Select + 'AND ' + 'Giornata = ' + CAST(@Giornata AS varchar(60)) + ' ';
		End
		ELSE
		Begin
			SET @Select = @Select + 'Giornata = ' + CAST(@Giornata AS varchar(60)) + ' ';
			SET @Cont = 1
		End
	End
	print @Select	
	EXECUTE(@Select)
	/*Brutto ma funziona!*/
	--IF(DATEDIFF(DD, @Data, '2000-01-01 00:00:00')=0)
	--Begin
	--	SET @Data = NULL
	--End

	--IF(@SquadraCasaId = 0)
	--Begin
	--	SET @SquadraCasaId = NULL
	--End

	--IF(@SquadraOspiteId = 0)
	--Begin
	--	SET @SquadraOspiteId = NULL
	--End

	--IF(@Giornata = 0)
	--Begin
	--	SET @Giornata = NULL
	--End

	
	--IF(@Arbitro1 = 0 OR @Arbitro2 = 0)
	--Begin

	--SET @Arbitro1 = NULL

	--SET @Arbitro2 = NULL

	--SELECT * FROM Calendario WHERE Id = isnull(@Id,Id) AND Girone = isnull(@Girone, Girone) AND Campionato = isnull(@Campionato, Campionato)
	--							   AND Categoria = isnull(@Categoria, Categoria) AND TipoGiornata = isnull(@TipoGiornata, TipoGiornata)
	--							   AND Data = isnull(@Data, Data) AND SquadraCasaId = isnull(@SquadraCasaId, SquadraCasaId)
	--							   AND SquadraOspiteId = isnull(@SquadraOspiteId, SquadraOspiteId)
	--							   AND Giornata = isnull(@Giornata, Giornata) AND Ora = isnull(@Ora, Ora)
	--End
	--IF(@Arbitro1 != NULL OR @Arbitro2 != NULL)
	--Begin
	--print @Arbitro1
	--print @Arbitro2
	--SELECT * FROM Calendario WHERE Id = isnull(@Id,Id) AND Girone = isnull(@Girone, Girone) AND Campionato = isnull(@Campionato, Campionato)
	--							   AND Categoria = isnull(@Categoria, Categoria) AND TipoGiornata = isnull(@TipoGiornata, TipoGiornata)
	--							   AND Data = isnull(@Data, Data) AND SquadraCasaId = isnull(@SquadraCasaId, SquadraCasaId)
	--							   AND SquadraOspiteId = isnull(@SquadraOspiteId, SquadraOspiteId)
	--							   AND Giornata = isnull(@Giornata, Giornata) AND Ora = isnull(@Ora, Ora) AND Arbitro1Id = isnull(@Arbitro1, Arbitro1Id)
	--							   AND Arbitro2Id = ISNULL(@Arbitro2, Arbitro2Id) 
	--End

	--SELECT * FROM Calendario WHERE Id = isnull(@Id,Id) AND Girone = isnull(@Girone, Girone) AND Campionato = isnull(@Campionato, Campionato)
	--							   AND Categoria = isnull(@Categoria, Categoria) AND TipoGiornata = isnull(@TipoGiornata, TipoGiornata)
	--							   AND Data = isnull(@Data, Data) AND SquadraCasaId = isnull(@SquadraCasaId, SquadraCasaId)
	--							   AND SquadraOspiteId = isnull(@SquadraOspiteId, SquadraOspiteId)
	--							   AND Giornata = isnull(@Giornata, Giornata) AND Ora = isnull(@Ora, Ora) AND Arbitro1Id = isnull(@Arbitro1, Arbitro1Id)
	--							   AND Arbitro2Id = ISNULL(@Arbitro2, Arbitro2Id)

END
GO
/****** Object:  StoredProcedure [dbo].[filterSquadra]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[filterSquadra]

	@Id int = null,
	@NomeSquadra varchar(50) = null,
	@Girone varchar(2) = null,
	@Categoria varchar (20) = null
AS
BEGIN
	
	IF(@Id = 0)
	Begin
		SET @Id = NULL
	End

	SELECT * FROM Squadra WHERE Id= isnull(@Id, Id) AND NomeSquadra = isnull(@NomeSquadra, NomeSquadra) 
								AND Girone = isnull(@Girone, Girone) AND Categoria = isnull(@Categoria, Categoria)
					             

END
GO
/****** Object:  StoredProcedure [dbo].[filterUtente]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[filterUtente]

	@IdUtente int = null,
	@Nome varchar(20) = null,
	@Cognome varchar(20) = null,
	@Email nvarchar(100) = null,
	@Ruolo nvarchar(10) = null

AS
BEGIN
	
	IF(@IdUtente = 0)
	Begin
		Set @IdUtente = NULL
	End

	SELECT * FROM Utente WHERE Id = isnull(@IdUtente, Id) AND Nome = isnull(@Nome, Nome) And 
	                           Cognome = isnull(@Cognome, Cognome) AND Email =isnull(@Email, Email)
							   AND Ruolo = ISNULL(@Ruolo, Ruolo)
END
GO
/****** Object:  StoredProcedure [dbo].[insertArbitro]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertArbitro]

@Tessera int,
@Cognome nvarchar(50),
@Nome nvarchar(50),
@Categoria varchar(10),
@Qualifica nvarchar(100),
@Abilitazione nvarchar(50),
@Email nvarchar(100),
@NPartiteArbitrate int = 0

AS
BEGIN
	
	DECLARE @IdArbitro int;

	 INSERT INTO Arbitro(Tessera, Cognome, Nome, Categoria, Qualifica,
    Abilitazione, Email, NPartiteArbitrate) values (@Tessera, @Cognome, 
    @Nome, @Categoria, @Qualifica, @Abilitazione, @Email, @NPartiteArbitrate)
    
	SET @IdArbitro = (select SCOPE_IDENTITY() as ID)
	
	SELECT * FROM Arbitro WHERE Id= @IdArbitro

END
GO
/****** Object:  StoredProcedure [dbo].[insertCalendario]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertCalendario]

@Girone varchar(3),
@Campionato varchar(15),
@Categoria varchar(15),
@TipoGiornata varchar(10),
@Data date,
@SquadraCasaId int,
@SquadraOspiteId int,
@Campo nvarchar(50),
@Indirizzo nvarchar(100),
@GiornoPartita varchar(10),
@Citta varchar(20),
@Provincia varchar(20),
@Ora nvarchar(8),
@Giornata int

AS
BEGIN

	DECLARE @IdCal int
	
	INSERT INTO Calendario(Girone, Campionato, Categoria, TipoGiornata, Data, SquadraCasaId, SquadraOspiteId,
                        Campo, Indirizzo, GiornoPartita, Citta, Provincia, Ora, Giornata)
                        values (@Girone, @Campionato, @Categoria, @TipoGiornata, @Data, @SquadraCasaId, @SquadraOspiteId,
                        @Campo, @Indirizzo, @GiornoPartita, @Citta, @Provincia, @Ora, @Giornata)
    SET @IdCal = (select SCOPE_IDENTITY() as ID)
	
	SELECT * FROM Calendario WHERE Id= @IdCal

END
GO
/****** Object:  StoredProcedure [dbo].[insertEccezione]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertEccezione]
	@IdArbitro int,
	@DataInizio date = null,
	@DataFine date = null,
	@SquadraConflitto int = null,
	@Note varchar(50)= null
AS
BEGIN
	IF(DATEDIFF(DD,@DataInizio , '2000-01-01 00:00:00')=0)
	Begin
		SET @DataInizio = NULL
	End

	IF(DATEDIFF(DD,@DataFine , '2000-01-01 00:00:00')=0)
	Begin
		SET @DataFine = NULL
	End

	IF(@SquadraConflitto = 0)
	Begin
		SET @SquadraConflitto = 0
	End

	DECLARE @IdEccezione int;

	INSERT INTO Eccezione (DataInizio, DataFine, SquadraId, Note) 
												VALUES(@DataInizio, @DataFine, @SquadraConflitto, @Note)
	SET @IdEccezione = 	(SELECT SCOPE_IDENTITY() AS ID)

	INSERT INTO ArbitroEccezione(ArbitroId, EccezioneId) VALUES (@IdArbitro, @IdEccezione)
END
GO
/****** Object:  StoredProcedure [dbo].[insertSquadra]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertSquadra]
	
	@NomeSquadra nvarchar(100),
	@Girone varchar(3),
	@Categoria varchar(15)

AS
BEGIN
	
	DECLARE @IdSquadra int;

	insert into Squadra(NomeSquadra, Girone, Categoria)
	values (@NomeSquadra, @Girone, @Categoria)

	SET @IdSquadra = (select SCOPE_IDENTITY() as ID)
	
	SELECT * FROM Squadra WHERE Id= @IdSquadra

END
GO
/****** Object:  StoredProcedure [dbo].[login]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[login]

	@Email nvarchar(50),
	@Password nvarchar(50)

AS
BEGIN

	DECLARE @IdUtente int;
	
	SELECT * FROM Utente WHERE Email = @Email AND Password = @Password

	SET @IdUtente = (select SCOPE_IDENTITY() as ID)
	
	SELECT * FROM Utente WHERE Id= @IdUtente

END
GO
/****** Object:  StoredProcedure [dbo].[registrazione]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[registrazione]

	@Nome varchar(50),
	@Cognome varchar (50),
	@Email nvarchar (100),
	@Password nvarchar (50)
	
AS
BEGIN
	
	INSERT INTO Utente (Nome, Cognome, Email, Password, Ruolo) VALUES (@Nome, @Cognome, @Email, @Password, 'USER')

END
GO
/****** Object:  StoredProcedure [dbo].[selectAllArbitro]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAllArbitro]
AS
BEGIN
	SELECT * FROM Arbitro
END
GO
/****** Object:  StoredProcedure [dbo].[selectAllCalendario]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAllCalendario]
AS
BEGIN
	
	SELECT * FROM Calendario

END
GO
/****** Object:  StoredProcedure [dbo].[selectAllSquadra]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAllSquadra]
AS
BEGIN
	SELECT * FROM Squadra
END
GO
/****** Object:  StoredProcedure [dbo].[selectAllUtente]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAllUtente]
AS
BEGIN
	SELECT * FROM Utente
END
GO
/****** Object:  StoredProcedure [dbo].[updateArbitro]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateArbitro]

@IdArbitro int = null,
@Tessera int = null,
@Cognome nvarchar(50) = null,
@Nome nvarchar(50) = null,
@Categoria varchar(10) = null,
@Qualifica nvarchar(100) = null,
@Abilitazione nvarchar(50) = null,
@Email nvarchar(100) = null,
@PartiteArbitrate int = null

AS
BEGIN
	
	DECLARE @RSet int;

	IF(@Tessera = 0)
	Begin
		SET @Tessera = null
	End

	IF(@IdArbitro = 0)
	Begin
		SET @IdArbitro= null
	End

	UPDATE Arbitro SET Tessera = isnull(@Tessera, Tessera), Cognome = isnull(@Cognome, Cognome),Nome = isnull(@Nome, Nome),
					   Categoria = isnull(@Categoria, Categoria), Qualifica = isnull(@Qualifica, Qualifica), 
					   Abilitazione = isnull(@Abilitazione, Abilitazione), Email =  isnull(@Email, Email), 
					   NPartiteArbitrate = isnull(@PartiteArbitrate, NPartiteArbitrate)
					   where Arbitro.Id = @IdArbitro
	SELECT * FROM Arbitro WHERE @IdArbitro = Id

END
GO
/****** Object:  StoredProcedure [dbo].[updateCalendario]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateCalendario]

@Id int,
@Girone varchar(3) = null,
@Campionato varchar(15) = null,
@Categoria varchar(15) = null,
@TipoGiornata varchar(10) = null,
@Data date = null,
@SquadraCasaId int = null,
@SquadraOspiteId int = null,
@Campo nvarchar(50) = null,
@Indirizzo nvarchar(100) = null,
@GiornoPartita varchar(10) = null,
@Citta varchar(20) = null,
@Provincia varchar(20) = null,
@Ora nvarchar(8) = null,
@Giornata int = null

AS
BEGIN
	/*Brutto ma funziona!*/
	IF(DATEDIFF(DD, @Data, '2000-01-01 00:00:00')=0)
	Begin
		SET @Data = null
	End

	IF(@SquadraCasaId = 0)
	Begin
		SET @SquadraCasaId = NULL
	End

	IF(@SquadraOspiteId = 0)
	Begin
		SET @SquadraOspiteId = NULL
	End

	IF(@Giornata = 0)
	Begin
		SET @Giornata = NULL
	End
	
	update Calendario set Girone = isnull(@Girone, Girone), Campionato = isnull(@Campionato, Campionato), 
						  Categoria = isnull(@Categoria, Categoria), TipoGiornata = isnull(@TipoGiornata, TipoGiornata), 
						  Data = isnull(@Data, Data), SquadraCasaId = isnull(@SquadraCasaId, SquadraCasaId), 
						  SquadraOspiteId = isnull(@SquadraOspiteId, SquadraOspiteId), Campo = isnull(@Campo, Campo), 
						  Indirizzo = isnull(@Indirizzo, Indirizzo), GiornoPartita = isnull(@GiornoPartita, GiornoPartita), 
						  Citta = isnull(@Citta, Citta), Provincia = isnull(@Provincia, Provincia), Ora = isnull(@Ora, Ora),
						  Giornata = isnull(@Giornata, Giornata)
						  where Calendario.Id = @Id 
	SELECT * FROM Calendario WHERE id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[updateSquadra]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateSquadra]

@IdSquadra int = null,
@NomeSquadra nvarchar(100) = null,
@Girone varchar(3) = null,
@Categoria varchar(15) = null

AS
BEGIN

	update Squadra set NomeSquadra = isnull(@NomeSquadra, NomeSquadra), Girone = isnull(@Girone, Girone),
	Categoria = isnull(@Categoria, Categoria)
	where Squadra.Id = @IdSquadra
	
	SELECT * FROM Squadra WHERE Id = @IdSquadra

END
GO
/****** Object:  StoredProcedure [dbo].[updateUtente]    Script Date: 25/10/2021 15:58:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateUtente]
	
	@IdUser int,
	@Nome varchar(50) = null,
	@Cognome varchar(50) = null,
	@Email nvarchar(50) = null,
	@Password nvarchar(50) = null,
	@Ruolo varchar(10) = null

AS
BEGIN
	
	UPDATE Utente SET Nome = isnull(@Nome, Nome),Cognome =isnull(@Cognome, Cognome), 
				      Email = isnull(@Email, Email), Password = isnull(@Password, Password), Ruolo = isnull(@Ruolo, Ruolo)
					  WHERE Id = @IdUser

END
GO
