﻿using AssegnazioneArbitri.Models;
using AssegnazioneArbitri.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssegnazioneArbitri.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UtenteController : ControllerBase
    {

        private readonly IUtente UtenteRepo;

        public UtenteController(IUtente Utente)
        {
            UtenteRepo = Utente;
        }

        [HttpPost("filterUtente")]
        public List<Dictionary<string, object>> filterUtente([FromBody] Utente utente)
        {
            return UtenteRepo.filterUtente(utente);
        }

        [HttpGet("login")]
        public List<Dictionary<string, object>> loginUtente([FromBody] Utente utente)
        {
            return UtenteRepo.loginUtente(utente);
        }

        [HttpPost("register")]
        public List<Dictionary<string, object>> registerUtente([FromBody] Utente utente)
        {
            return UtenteRepo.registerUtente(utente);
        }

        [HttpGet("updateUtente")]
        public List<Dictionary<string, object>> updateUtente([FromBody] Utente utente)
        {
            return UtenteRepo.updateUtente(utente);
        }

        [HttpGet("getAllUtente")]
        public List<Dictionary<string, object>> getAllUtente()
        {
            return UtenteRepo.getAllUtente();
        }

    }
}
