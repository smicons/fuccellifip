﻿using AssegnazioneArbitri.Models;
using AssegnazioneArbitri.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssegnazioneArbitri.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EccezioneController : ControllerBase
    {

        private readonly IEccezione EccezioneRepo;

        public EccezioneController(IEccezione eccezione)
        {
            EccezioneRepo = eccezione;
        }

        [HttpPost("insertEccezione/{IdArbitro}")]
        public void insertSquadra([FromBody]Eccezione eccezione, int IdArbitro)
        {

            if (eccezione.DataInizio == DateTime.Parse("01 / 01 / 0001 00:00:00"))
            {
                eccezione.DataInizio = DateTime.Parse("2000-01-01T00:00:00");
            }

            if (eccezione.DataFine == DateTime.Parse("01 / 01 / 0001 00:00:00"))
            {
                eccezione.DataFine = DateTime.Parse("2000-01-01T00:00:00");
            }

            EccezioneRepo.insertEccezione(eccezione, IdArbitro);
        }

    }
}
