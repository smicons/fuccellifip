﻿using AssegnazioneArbitri.Models;
using AssegnazioneArbitri.Repository.RepositoryImpl;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssegnazioneArbitri.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CalendarioController : ControllerBase
    {

        private readonly ICalendario CalendarioRepo;

        public CalendarioController(ICalendario calendario)
        {
            CalendarioRepo = calendario;
        }

        [HttpPost("filterCalendario")]
        public List<Dictionary<string, object>> filterCalendario([FromBody] Calendario calendario)
        {
            if (calendario.Data == DateTime.Parse("01 / 01 / 0001 00:00:00"))
            {
                calendario.Data = DateTime.Parse("2000-01-01T00:00:00");
            }
            return CalendarioRepo.filterCalendario(calendario);
        }

        [HttpPost("insertCalendario")]
        public List<Dictionary<string, object>> insertCalendario([FromBody] Calendario calendario)
        {
            return CalendarioRepo.insertCalendario(calendario);
        }

        [HttpPut("updateCalendario")]
        public List<Dictionary<string, object>> updateCalendario([FromBody] Calendario calendario)
        {
            if(calendario.Data == DateTime.Parse("01 / 01 / 0001 00:00:00"))
            {
                calendario.Data = DateTime.Parse("2000-01-01T00:00:00");
            }
            return CalendarioRepo.updateCalendario(calendario);
        }

        [HttpDelete("deleteCalendario/{idCalendario}")]
        public List<Dictionary<string, object>> filterCalendario(int idCalendario)
        {
            return CalendarioRepo.deleteCalendario(idCalendario);
        }

        [HttpPut("randomInsertAll")]
        public List<Dictionary<string, object>> randomInsertAll()
        {
            return CalendarioRepo.randomInsertArbitri();
        }

        [HttpPut("randomInsertOne/{idCalendario}")]
        public List<Dictionary<string, object>> randomInsertOne(int idCalendario)
        {
            return CalendarioRepo.randomInsertOne(idCalendario);
        }

    }
}
