﻿using AssegnazioneArbitri.Models;
using AssegnazioneArbitri.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssegnazioneArbitri.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArbitroController : ControllerBase
    {

        private readonly IArbitro ArbitroRepo;

        public ArbitroController(IArbitro arbitro)
        {
            ArbitroRepo = arbitro;
        }

        [HttpPost("filterArbitro")]
        public List<Dictionary<string, object>> filterArbitro([FromBody] Arbitro arbitro)
        {
            return ArbitroRepo.filterArbitro(arbitro);
        }

        [HttpPost("insertArbitro")]
        public List<Dictionary<string, object>> insertArbitro([FromBody] Arbitro arbitro)
        {
            return ArbitroRepo.insertArbitro(arbitro);
        }

        [HttpPut("updateArbitro")]
        public List<Dictionary<string, object>> updateArbitro([FromBody] Arbitro arbitro)
        {
            return ArbitroRepo.updateArbitro(arbitro);
        }

        [HttpDelete("deleteArbitro/{idArbitro}")]
        public List<Dictionary<string, object>> deleteArbitro(int idArbitro)
        {
            return ArbitroRepo.deleteArbitro(idArbitro);
        }

        [HttpGet("selectAllArbitro")]
        public List<Dictionary<string, object>> selectAllArbitro()
        {
            return ArbitroRepo.selectAllArbitro();
        }

    }
}
