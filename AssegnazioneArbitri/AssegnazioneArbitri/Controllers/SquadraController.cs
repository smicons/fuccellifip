﻿using AssegnazioneArbitri.Models;
using AssegnazioneArbitri.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssegnazioneArbitri.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SquadraController : ControllerBase
    {
        private readonly ISquadra SquadraRepo;

        public SquadraController(ISquadra squadra)
        {
            SquadraRepo = squadra;
        }

        [HttpPost("filterSquadra")]
        public List<Dictionary<string, object>> filterSquadra(Squadra squadra)
        {
            return SquadraRepo.filterSquadra(squadra);
        }

        [HttpPost("insertSquadra")]
        public List<Dictionary<string, object>> insertSquadra(Squadra squadra)
        {
            return SquadraRepo.insertSquadra(squadra);
        }

        [HttpPut("updateSquadra")]
        public List<Dictionary<string, object>> updateSquadra(Squadra squadra)
        {
            return SquadraRepo.updateSquadra(squadra);
        }

        [HttpDelete("deleteSquadra/{idSquadra}")]
        public List<Dictionary<string, object>> deleteSquadra(int idSquadra)
        {
            return SquadraRepo.deleteSquadra(idSquadra);
        }

        [HttpGet("selectAllSquadra")]
        public List<Dictionary<string, object>> selectAllArbitro()
        {
            return SquadraRepo.selectAllSquadra();
        }
    }
}
