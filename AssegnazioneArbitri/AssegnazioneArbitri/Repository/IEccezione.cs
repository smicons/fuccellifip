﻿using AssegnazioneArbitri.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssegnazioneArbitri.Repository
{
    public interface IEccezione
    {

        void insertEccezione(Eccezione eccezione, int IdArbitro);

    }
}
