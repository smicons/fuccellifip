﻿using AssegnazioneArbitri.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssegnazioneArbitri.Repository.RepositoryImpl
{
    public interface ICalendario
    {

        List<Dictionary<string, object>> filterCalendario(Calendario c);

        List<Dictionary<string, object>> insertCalendario(Calendario c);

        List<Dictionary<string, object>> updateCalendario(Calendario c);

        List<Dictionary<string, object>> deleteCalendario(int idCalendario);

        List<Dictionary<string, object>> randomInsertArbitri();

        List<Dictionary<string, object>> randomInsertOne(int idCalendario);

    }
}
