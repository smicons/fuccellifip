﻿using AssegnazioneArbitri.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssegnazioneArbitri.Repository
{
    public interface IArbitro
    {

        List<Dictionary<string, object>> filterArbitro(Arbitro a);

        List<Dictionary<string, object>> insertArbitro(Arbitro a);

        List<Dictionary<string, object>> deleteArbitro(int idArbitro);

        List<Dictionary<string, object>> updateArbitro(Arbitro a);

        List<Dictionary<string, object>> selectAllArbitro();

    }
}
