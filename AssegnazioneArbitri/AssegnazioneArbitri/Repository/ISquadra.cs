﻿using AssegnazioneArbitri.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssegnazioneArbitri.Repository
{
    public interface ISquadra
    {

        List<Dictionary<string, object>> filterSquadra(Squadra s);

        List<Dictionary<string, object>> insertSquadra(Squadra s);

        List<Dictionary<string, object>> updateSquadra(Squadra s);

        List<Dictionary<string, object>> deleteSquadra(int idSquadra);
        List<Dictionary<string, object>> selectAllSquadra();
    }
}
