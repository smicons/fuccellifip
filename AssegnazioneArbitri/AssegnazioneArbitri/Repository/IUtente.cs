﻿using AssegnazioneArbitri.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssegnazioneArbitri.Repository
{
    public interface IUtente
    {

        List<Dictionary<string, object>> filterUtente(Utente u);

        List<Dictionary<string, object>> registerUtente(Utente u);

        List<Dictionary<string, object>> updateUtente(Utente u);

        List<Dictionary<string, object>> loginUtente(Utente u);

        List<Dictionary<string, object>> getAllUtente();

    }
}
