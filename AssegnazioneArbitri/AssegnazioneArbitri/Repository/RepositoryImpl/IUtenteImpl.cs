﻿using AssegnazioneArbitri.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssegnazioneArbitri.Repository.RepositoryImpl
{
    public class IUtenteImpl : AbstractRepository, IUtente
    {
        public IUtenteImpl(IDbContext _context) : base(_context) { }

        public List<Dictionary<string, object>> filterUtente(Utente u)
        {
            var data = ExecuteStore("filterutente", new List<ModelQueryParam>()
            {
                new ModelQueryParam("@IdUtente", u.Id),
                new ModelQueryParam("@Nome", u.Nome),
                new ModelQueryParam("@Cognome", u.Cognome),
                new ModelQueryParam("@Email", u.Email),
                new ModelQueryParam("@Ruolo", u.Ruolo)
            });

            return data;
        }

        public List<Dictionary<string, object>> registerUtente(Utente u)
        {
            var data = ExecuteStore("registrazione", new List<ModelQueryParam>()
            {
                new ModelQueryParam("@Nome", u.Nome),
                new ModelQueryParam("@Cognome", u.Cognome),
                new ModelQueryParam("@Email", u.Email),
                new ModelQueryParam("@Password", u.Password)
            });

            return data;
        }

        public List<Dictionary<string, object>> updateUtente(Utente u)
        {
            var data = ExecuteStore("updateUtente", new List<ModelQueryParam>()
            {
                new ModelQueryParam("@IdUtente", u.Id),
                new ModelQueryParam("@Nome", u.Nome),
                new ModelQueryParam("@Cognome", u.Cognome),
                new ModelQueryParam("@Email", u.Email),
                new ModelQueryParam("@Ruolo", u.Ruolo)
            });

            return data;
        }

        public List<Dictionary<string, object>> loginUtente(Utente u)
        {
            var data = ExecuteStore("login", new List<ModelQueryParam>()
            {

                new ModelQueryParam("@Password", u.Password),
                new ModelQueryParam("@Email", u.Email)

            });

            return data;
        }

        public List<Dictionary<string, object>> getAllUtente()
        {
            var data = ExecuteStore("selectAllUtente", new List<ModelQueryParam>());

            return data;
        }
    }
}
