﻿using AssegnazioneArbitri.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssegnazioneArbitri.Repository.RepositoryImpl
{
    public class ISquadraImpl : AbstractRepository, ISquadra
    {

        public ISquadraImpl(IDbContext _context) : base(_context) { }

        public List<Dictionary<string, object>> deleteSquadra(int idSquadra)
        {
            var data = ExecuteStore("deleteSquadra", new List<ModelQueryParam>()
            {

                new ModelQueryParam("@Id", idSquadra)

            });

            return data;
        }

        public List<Dictionary<string, object>> filterSquadra(Squadra s)
        {
            var data = ExecuteStore("filterSquadra", new List<ModelQueryParam>()
            {

                new ModelQueryParam("@Id", s.Id),
                new ModelQueryParam("@NomeSquadra", s.NomeSquadra),
                new ModelQueryParam("@Girone", s.Girone),
                new ModelQueryParam("@Categoria", s.Categoria)

            });

            return data;
        }

        public List<Dictionary<string, object>> insertSquadra(Squadra s)
        {
            var data = ExecuteStore("insertSquadra", new List<ModelQueryParam>()
            {

                new ModelQueryParam("@NomeSquadra", s.NomeSquadra),
                new ModelQueryParam("@Girone", s.Girone),
                new ModelQueryParam("@Categoria", s.Categoria)

            });

            return data;
        }

        public List<Dictionary<string, object>> selectAllSquadra()
        {
            var data = ExecuteStore("selectAllSquadra", new List<ModelQueryParam>());

            return data;
        }

        public List<Dictionary<string, object>> updateSquadra(Squadra s)
        {
            var data = ExecuteStore("updateSquadra", new List<ModelQueryParam>()
            {

                new ModelQueryParam("@IdSquadra", s.Id),
                new ModelQueryParam("@NomeSquadra", s.NomeSquadra),
                new ModelQueryParam("@Girone", s.Girone),
                new ModelQueryParam("@Categoria", s.Categoria)

            });

            return data;
        }
    }
}
