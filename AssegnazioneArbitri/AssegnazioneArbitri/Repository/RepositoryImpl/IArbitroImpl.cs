﻿using AssegnazioneArbitri.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssegnazioneArbitri.Repository.RepositoryImpl
{
    public class IArbitroImpl : AbstractRepository, IArbitro
    {

        public IArbitroImpl(IDbContext _context) : base(_context) { }

        public List<Dictionary<string, object>> deleteArbitro(int idArbitro)
        {
            var data = ExecuteStore("deleteArbitro", new List<ModelQueryParam>()
            {

                new ModelQueryParam("@Id", idArbitro)

            });

            return data;
        }

        public List<Dictionary<string, object>> filterArbitro(Arbitro a)
        {
            var data = ExecuteStore("filterArbitro", new List<ModelQueryParam>()
            {

                new ModelQueryParam("@Id", a.Id),
                new ModelQueryParam("@Nome", a.Nome),
                new ModelQueryParam("@Cognome", a.Cognome),
                new ModelQueryParam("@Tessera", a.Tessera),
                new ModelQueryParam("@Qualifica", a.Qualifica),
                new ModelQueryParam("@Categoria", a.Categoria),
                new ModelQueryParam("@Abilitazione", a.Abilitazione),
                new ModelQueryParam("@Email", a.Email)

            });

            return data;
        }

        public List<Dictionary<string, object>> insertArbitro(Arbitro a)
        {
            var data = ExecuteStore("insertArbitro", new List<ModelQueryParam>()
            {

                new ModelQueryParam("@Nome", a.Nome),
                new ModelQueryParam("@Cognome", a.Cognome),
                new ModelQueryParam("@Tessera", a.Tessera),
                new ModelQueryParam("@Qualifica", a.Qualifica),
                new ModelQueryParam("@Categoria", a.Categoria),
                new ModelQueryParam("@Abilitazione", a.Abilitazione),
                new ModelQueryParam("@Email", a.Email),
                new ModelQueryParam("@NPartiteArbitrate", a.NPartiteArbitrate)

            });

            return data;
        }

        public List<Dictionary<string, object>> selectAllArbitro()
        {
            var data = ExecuteStore("selectAllArbitro", new List<ModelQueryParam>());

            return data;
        }

        public List<Dictionary<string, object>> updateArbitro(Arbitro a)
        {
            var data = ExecuteStore("updateArbitro", new List<ModelQueryParam>()
            {

                new ModelQueryParam("@IdArbitro", a.Id),
                new ModelQueryParam("@Nome", a.Nome),
                new ModelQueryParam("@Cognome", a.Cognome),
                new ModelQueryParam("@Tessera", a.Tessera),
                new ModelQueryParam("@Qualifica", a.Qualifica),
                new ModelQueryParam("@Categoria", a.Categoria),
                new ModelQueryParam("@Abilitazione", a.Abilitazione),
                new ModelQueryParam("@Email", a.Email)

            });

            return data;
        }
    }
}
