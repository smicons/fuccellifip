﻿using AssegnazioneArbitri.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssegnazioneArbitri.Repository.RepositoryImpl
{
    public class ICalendarioImpl : AbstractRepository, ICalendario
    {

        public ICalendarioImpl(IDbContext _context) : base(_context) { }

        public List<Dictionary<string, object>> deleteCalendario(int idCalendario)
        {

            var data = ExecuteStore("deleteCalendario", new List<ModelQueryParam>()
            {

                new ModelQueryParam("@Id", idCalendario)

            });

            return data;

        }

        public List<Dictionary<string, object>> filterCalendario(Calendario c)
        {
            var data = ExecuteStore("filterCalendario", new List<ModelQueryParam>()
            {

                new ModelQueryParam("@Id", c.Id),
                new ModelQueryParam("@Girone", c.Girone),
                new ModelQueryParam("@Campionato", c.Campionato),
                new ModelQueryParam("@Categoria", c.Categoria),
                new ModelQueryParam("@TipoGiornata", c.TipoGiornata),
                new ModelQueryParam("@Data", c.Data),
                new ModelQueryParam("@SquadraCasaId", c.SquadraCasaId),
                new ModelQueryParam("@SquadraOspiteId", c.SquadraOspiteId),
                new ModelQueryParam("@Arbitro1Id", c.Arbitro1Id),
                new ModelQueryParam("@Giornata", c.Giornata),
                new ModelQueryParam("@Ora", c.Ora)

            });

            return data;
        }

        public List<Dictionary<string, object>> insertCalendario(Calendario c)
        {
            var data = ExecuteStore("insertCalendario", new List<ModelQueryParam>()
            {

                new ModelQueryParam("@Girone", c.Girone),
                new ModelQueryParam("@Campionato", c.Campionato),
                new ModelQueryParam("@Categoria", c.Categoria),
                new ModelQueryParam("@TipoGiornata", c.TipoGiornata),
                new ModelQueryParam("@Data", c.Data),
                new ModelQueryParam("@SquadraCasaId", c.SquadraCasaId),
                new ModelQueryParam("@SquadraOspiteId", c.SquadraOspiteId),
                new ModelQueryParam("@Campo", c.Campo),
                new ModelQueryParam("@Indirizzo", c.Indirizzo),
                new ModelQueryParam("@GiornoPartita", c.GiornoPartita),
                new ModelQueryParam("@Citta", c.Citta),
                new ModelQueryParam("@Provincia", c.Provincia),
                new ModelQueryParam("@Ora", c.Ora),
                new ModelQueryParam("@Giornata", c.Giornata),

            });

            return data;
        }

        public List<Dictionary<string, object>> randomInsertArbitri()
        {
            ExecuteStore("CicloPartite", new List<ModelQueryParam>());

            var data = ExecuteStore("selectAllCalendario", new List<ModelQueryParam>());

            return data;
        }

        public List<Dictionary<string, object>> randomInsertOne(int idCalendario)
        {
            ExecuteStore("ArbitriRandom", new List<ModelQueryParam>()
            {

                new ModelQueryParam("@IdPartita", idCalendario)

            });

            var data = ExecuteStore("selectAllCalendario", new List<ModelQueryParam>());

            return data;

        }

        public List<Dictionary<string, object>> updateCalendario(Calendario c)
        {
            var data = ExecuteStore("updateCalendario", new List<ModelQueryParam>()
            {

                 new ModelQueryParam("@Id", c.Id),
                new ModelQueryParam("@Girone", c.Girone),
                new ModelQueryParam("@Campionato", c.Campionato),
                new ModelQueryParam("@Categoria", c.Categoria),
                new ModelQueryParam("@TipoGiornata", c.TipoGiornata),
                new ModelQueryParam("@Data", c.Data),
                new ModelQueryParam("@SquadraCasaId", c.SquadraCasaId),
                new ModelQueryParam("@SquadraOspiteId", c.SquadraOspiteId),
                new ModelQueryParam("@Campo", c.Campo),
                new ModelQueryParam("@Indirizzo", c.Indirizzo),
                new ModelQueryParam("@GiornoPartita", c.GiornoPartita),
                new ModelQueryParam("@Citta", c.Citta),
                new ModelQueryParam("@Provincia", c.Provincia),
                new ModelQueryParam("@Ora", c.Ora),
                new ModelQueryParam("@Giornata", c.Giornata),

            });

            return data;
        }

    }
}
