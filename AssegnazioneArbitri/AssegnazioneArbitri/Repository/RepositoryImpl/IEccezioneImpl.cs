﻿using AssegnazioneArbitri.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssegnazioneArbitri.Repository.RepositoryImpl
{
    public class IEccezioneImpl : AbstractRepository, IEccezione
    {

        public IEccezioneImpl(IDbContext _context) : base(_context) { }

        public void insertEccezione(Eccezione eccezione, int IdArbitro)
        {
            ExecuteStore("insertEccezione", new List<ModelQueryParam>()
            {

                new ModelQueryParam("@DataInizio", eccezione.DataInizio),
                new ModelQueryParam("@DataFine", eccezione.DataFine),
                new ModelQueryParam("@SquadraConflitto", eccezione.SquadraId),
                new ModelQueryParam("@Note", eccezione.Note),
                new ModelQueryParam("@IdArbitro", IdArbitro)

            });
        }
    }
}
