﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace AssegnazioneArbitri.Models
{
    public class AbstractRepository
    {
        private IDbContext _context = null;
        public AbstractRepository(IDbContext context)
        {
            _context = context;
        }

        public List<Dictionary<string, object>> Execute(string name, params object[] parameters)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }
            using (var dbCon = _context.DB)
            using (var sqlCommand = new SqlCommand(name, dbCon))
            {
                if (dbCon.State == ConnectionState.Closed)
                {
                    dbCon.Open();
                }

                try
                {
                    //Build the sql command
                    sqlCommand.CommandType = CommandType.Text;

                    var reader = sqlCommand.ExecuteReader();
                    var columns = reader.GetColumnSchema();

                    //Format rows into List of dynamic objects
                    var result = new List<Dictionary<string, object>>();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var newObj = new Dictionary<string, object>();
                            for (int i = 0; i < columns.Count; i++)
                            {
                                var columnName = columns.ElementAt(i).ColumnName;
                                newObj.Add(columnName, reader.GetValue(i));
                            }
                            result.Add(newObj);
                        }
                        reader.Close();
                    }

                    if (dbCon.State == ConnectionState.Open)
                    {
                        dbCon.Close();
                    }

                    return result;
                }
                catch (Exception ex)
                {
                    dbCon.Close();
                    throw ex;
                }
            }
        }

        public List<Dictionary<string, object>> ExecuteStore(string name, List<ModelQueryParam> variables)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }
            using (var dbCon = _context.DB)
            using (var sqlCommand = new SqlCommand(name, dbCon))
            {
                if (dbCon.State == ConnectionState.Closed)
                {
                    dbCon.Open();
                }

                try
                {
                    //Build the sql command
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    foreach (var variable in variables)
                    {
                        if (!string.IsNullOrEmpty(variable.Name))
                        {
                            sqlCommand.Parameters.AddWithValue(variable.Name, variable.Value);
                        }
                    }

                    var reader = sqlCommand.ExecuteReader();
                    var columns = reader.GetColumnSchema();

                    //Format rows into List of dynamic objects
                    var result = new List<Dictionary<string, object>>();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var newObj = new Dictionary<string, object>();
                            for (int i = 0; i < columns.Count; i++)
                            {
                                var columnName = columns.ElementAt(i).ColumnName;
                                newObj.Add(columnName, reader.GetValue(i));
                            }
                            result.Add(newObj);
                        }
                        reader.Close();
                    }

                    if (dbCon.State == ConnectionState.Open)
                    {
                        dbCon.Close();
                    }

                    return result;
                }
                catch (Exception ex)
                {
                    dbCon.Close();
                    throw ex;
                }
            }
        }
    }
}
