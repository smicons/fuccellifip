﻿using AssegnazioneArbitri.Infrastucture;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssegnazioneArbitri.Models
{
    public class DbContex : IDbContext
    {
        private AppSetting _appSettings { get; set; }

        public DbContex(IOptions<AppSetting> settings)
        {
            _appSettings = settings.Value;
        }

        public SqlConnection DB { get => new SqlConnection(_appSettings.DbConnectionString); }
    }
}
