﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssegnazioneArbitri.Models
{
    public class Arbitro
    {

        public int Id { get; set; }
        public int Tessera { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string Categoria { get; set; }
        public string Qualifica { get; set; }
        public string Abilitazione { get; set; }
        public string Email { get; set; }
        public int NPartiteArbitrate { get; set;}

    }
}
