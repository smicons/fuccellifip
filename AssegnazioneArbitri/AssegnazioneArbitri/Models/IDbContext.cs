﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssegnazioneArbitri.Models
{
    public interface IDbContext
    {
        SqlConnection DB { get; }
    }
}
