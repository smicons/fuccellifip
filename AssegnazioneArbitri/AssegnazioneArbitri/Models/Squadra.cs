﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssegnazioneArbitri.Models
{
    public class Squadra
    {

        public int Id { get; set; }
        public string NomeSquadra { get; set; }
        public string Girone { get; set; }
        public string Categoria { get; set; }

    }
}
