﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssegnazioneArbitri.Models
{
    public class Calendario
    {

        public int Id { get; set; }
        public string Girone { get; set; }
        public string Campionato { get; set; }
        public string Categoria { get; set; }
        public string TipoGiornata { get; set; }
        public DateTime Data { get; set; }
        public int SquadraCasaId { get; set; }
        public int SquadraOspiteId { get; set; }
        public int Arbitro1Id { get; set; }
        public int Arbitro2Id { get; set; }
        public string Campo { get; set; }
        public string Indirizzo { get; set; }
        public string GiornoPartita { get; set; }
        public string Citta { get; set; }
        public string Provincia { get; set; }
        public string Ora { get; set; }
        public int Giornata { get; set; }

    }
}
