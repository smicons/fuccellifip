﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssegnazioneArbitri.Infrastucture
{
    public class AppSetting
    {
        public string DbConnectionString { get; set; }
    }
}
